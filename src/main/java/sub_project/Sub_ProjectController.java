package sub_project;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.sql.Date;

import constants.Constants;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import project.Project;
import project.ProjectService;

@RestController
public class Sub_ProjectController {

	@Autowired
	private Sub_ProjectService srv;

	@Autowired
	private ProjectService psrv;
	
	@GetMapping("/subprojects")
	public @ResponseBody Collection<Sub_Project> findAll() {
		return srv.findAll();
	}
	
	@GetMapping(value = "/subprojects/{SID}")
	public @ResponseBody Sub_Project findByUsername(@PathVariable String SID) {
		try {
			int n = Integer.parseInt(SID);
			Sub_Project p = srv.findBySid(n);
			return p;
		} catch (Exception e) {
			return null;
		}
	}

	@GetMapping(value="/projects/{PID}/subprojects")
	public @ResponseBody Collection<Sub_Project> findAllByProject(@PathVariable String PID) {
		try{
			int n = Integer.parseInt(PID);
			Project p = psrv.findByPID(n);
			return srv.findAllByProject(p);
		} catch (Exception e) {
			return null;
		}
	}

	@PostMapping(value = "/subprojects")
	public @ResponseBody Sub_Project save(@RequestBody String subprojectJSON) {
		try {
			// Instantiate a sub project and save it.
			// Parse the JSON.
			JSONObject j = new JSONObject(subprojectJSON);
			// Populate all the data.
			int sid = (int) (System.currentTimeMillis() / 1000L);
			int pid = j.getInt(Constants.PARENT);
			Project parent = psrv.findByPID(pid);
			String sname = j.getString(Constants.SNAME);
			String status = j.getString(Constants.STATUS);

			// Parse & process the date.
			String dateString = j.getString(Constants.DUE_DATE);
			SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT);
			java.util.Date dateObject = format.parse(dateString);
			Date due_date =new java.sql.Date(dateObject.getTime());

			Sub_Project p = new Sub_Project(sid, parent, sname, status, due_date);
			return srv.save(p);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@PutMapping(value = "/subprojects/{SID}")
	public @ResponseBody Sub_Project updateSubProject(@PathVariable int SID, @RequestBody String subprojectJSON ){
		// Get current project info
		Sub_Project currentSubProject = srv.findBySid(SID);
		if(currentSubProject == null){
			System.out.println("Cant find project");
			return null;
		}

		try {
			// Instantiate a sub project and save it.
			// Parse the JSON.
			JSONObject j = new JSONObject(subprojectJSON);
			// Populate all the data.
			int pid = j.getInt(Constants.PARENT);
			Project parent = psrv.findByPID(pid);
			String sname = j.getString(Constants.SNAME);
			String status = j.getString(Constants.STATUS);

			// Parse & process the date.
			String dateString = j.getString(Constants.DUE_DATE);
			SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT);
			java.util.Date dateObject = format.parse(dateString);
			Date due_date =new java.sql.Date(dateObject.getTime());

			// Update the values
			currentSubProject.setProject(parent);
			currentSubProject.setSname(sname);
			currentSubProject.setStatus(status);
			currentSubProject.setDue_date(due_date);


			srv.save(currentSubProject);

			return currentSubProject;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	@DeleteMapping(value = "/subprojects/{SID}")
	public @ResponseBody void delete(@PathVariable int SID) {
		srv.delete(SID);
	}
}

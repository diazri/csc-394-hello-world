package asmt;

import com.example.demo.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;
import project.Project;
import usr.Usr;

import java.sql.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes = {DemoApplication.class})
@EnableJpaRepositories(basePackages = {"usr", "project", "sub_project", "comment","asmt","asmt_roster", "project_roster", "sub_project_roster"})
public class AsmtRepositoryIntegrationTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AsmtRepository asmtRepository;

    @Test
    public void whenFindByAID_thenReturnAsmt() {
        // given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(1, david, "p1", "inProgress", new Date(0));
        Asmt a1 = new Asmt(7, david, p1, "testAss");
        entityManager.persist(a1);
        entityManager.flush();

        // when
        Asmt found = asmtRepository.findByAid(a1.getAid());

        // then
        assertEquals(found.getAid(), a1.getAid());
    }

    @Test
    public void whenFindALL_thenReturnCorrectSize(){
        // when
        List<Asmt> found =asmtRepository.findAll();

        // then
        assertEquals(2, found.size());
    }

    @Test
    public void whenSave_thenReturnCorrectSize(){
        // given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(1, david, "p1", "inProgress", new Date(0));
        Asmt a1 = new Asmt(7, david, p1, "testAss");

        //when
        Asmt saved = asmtRepository.save(a1);
        List<Asmt> found = asmtRepository.findAll();

        // then
        assertEquals(3, found.size());
    }

    @Test
    public void whenSave_thenFindByAidReturnCorrectAID(){
        // given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(1, david, "p1", "inProgress", new Date(0));
        Asmt a1 = new Asmt(7, david, p1, "testAss");

        //when
        Asmt saved = asmtRepository.save(a1);
        Asmt found = asmtRepository.findByAid(7);

        // then
        assertEquals(a1.getAid(),found.getAid());
    }

    @Test
    public void whenSaveInvalidUser_thenFail(){
        // given
        Usr david = new Usr("kkg","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(1, david, "p1", "inProgress", new Date(0));
        Asmt a1 = new Asmt(7, david, p1, "testAss");

        //when
        try {
            Asmt saved = asmtRepository.save(a1);
            // Then
            fail();
        }
        catch (Exception e){}
    }

    @Test
    public void whenSaveInvalidProject_thenFail(){
        // given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(8, david, "p1", "inProgress", new Date(0));
        Asmt a1 = new Asmt(7, david, p1, "testAss");

        //when
        try {
            Asmt saved = asmtRepository.save(a1);
            // Then
            fail();
        }
        catch (Exception e){}
    }

    @Test
    public void whenDelete_thenFindAllSizeCorrect(){
        // given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(1, david, "p1", "inProgress", new Date(0));
        Asmt a1 = new Asmt(7, david, p1, "testAss");

        entityManager.persist(a1);
        entityManager.flush();

        //when
        asmtRepository.delete(a1);
        List<Asmt> found = asmtRepository.findAll();

        // then
        assertEquals(2, found.size());
    }
}

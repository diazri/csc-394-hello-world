package comment;

import asmt.Asmt;
import asmt.AsmtRepository;
import com.example.demo.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;
import project.Project;
import project.ProjectRepository;
import sub_project.Sub_Project;
import sub_project.Sub_ProjectRepository;
import usr.Usr;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes = {DemoApplication.class})
@EnableJpaRepositories(basePackages = {"usr", "project", "sub_project", "comment","asmt","asmt_roster", "project_roster", "sub_project_roster"})
public class CommentRepositoryIntegrationTests {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private Sub_ProjectRepository subProjectRepository;

    @Autowired
    private AsmtRepository asmtRepository;

    @Test
    public void whenFindByCID_thenReturnComment() {
        // given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        LocalDateTime ldt = LocalDateTime.now();


        Comment c1 = new Comment(7, david, "thumbsup", "Test Comment 1", ldt, null, null, null);
        entityManager.persist(c1);
        entityManager.flush();

        // when
        Comment found = commentRepository.findByCid(c1.getCid());

        // then
        assertEquals(found.getCid(), c1.getCid());
    }

    @Test
    public void whenFindALL_thenReturnCorrectSize(){
        // when
        List<Comment> found =commentRepository.findAll();

        // then
        assertEquals(2, found.size());
    }

    @Test
    public void whenSave_thenReturnCorrectSize(){
        // given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        LocalDateTime ldt = LocalDateTime.now();


        Comment c1 = new Comment(7, david, "thumbsup", "Test Comment 1", ldt, null, null, null);

        //when
        Comment saved = commentRepository.save(c1);
        List<Comment> found = commentRepository.findAll();

        // then
        assertEquals(3, found.size());
    }

    @Test
    public void whenSavewithProject_thenUpdateCorrectly(){
        // given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = projectRepository.findByPID(1);
        LocalDateTime ldt = LocalDateTime.now();

        // Construct and populate comment hash sets
        HashSet<Project> hp = new HashSet<>();
        hp.add(p1);

        Comment c1 = new Comment(7, david, "thumbsup", "Test Comment 1", ldt, hp, null, null);

        // when
        Comment saved = commentRepository.save(c1);
        entityManager.flush();
        Project found = projectRepository.findByPID(1);


        // then
        assertEquals(2, found.getComments().size());
    }

    @Test
    public void whenSavewithSubProject_thenUpdateCorrectly(){
        // given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Sub_Project sp1 = subProjectRepository.findBySid(1);
        LocalDateTime ldt = LocalDateTime.now();

        // Construct and populate comment hash sets

        HashSet<Sub_Project> hsp = new HashSet<>();
        hsp.add(sp1);


        Comment c1 = new Comment(7, david, "thumbsup", "Test Comment 1", ldt, null, hsp,  null);

        // when
        Comment saved = commentRepository.save(c1);
        entityManager.flush();
        Sub_Project found = subProjectRepository.findBySid(1);


        // then
        assertEquals(2, found.getComments().size());
    }
    @Test
    public void whenSavewithAsmt_thenUpdateCorrectly(){
        // given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Asmt a1 = asmtRepository.findByAid(1);
        LocalDateTime ldt = LocalDateTime.now();

        // Construct and populate comment hash sets
        HashSet<Asmt> ha = new HashSet<>();
        ha.add(a1);

        Comment c1 = new Comment(7, david, "thumbsup", "Test Comment 1", ldt, null, null, ha);

        // when
        Comment saved = commentRepository.save(c1);
        entityManager.flush();
        Asmt found = asmtRepository.findByAid(1);


        // then
        assertEquals(2, found.getComments().size());
    }
    @Test
    public void whenSave_thenFindByCidReturnCorrectCID(){
        // given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(1, david, "p1", "inProgress", new Date(0));
        Sub_Project sp1 = new Sub_Project(1, p1, "s1", "inProgress", new Date(0));
        Asmt a1 = new Asmt(1, david, p1, "testAss");
        LocalDateTime ldt = LocalDateTime.now();


        Comment c1 = new Comment(7, david, "thumbsup", "Test Comment 1", ldt, null, null, null);

        //when
        Comment saved = commentRepository.save(c1);
        Comment found = commentRepository.findByCid(7);

        // then
        assertEquals(c1.getCid(),found.getCid());
    }

    @Test
    public void whenSaveInvalidUser_thenFail(){
        // given
        Usr david = new Usr("kkg","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(1, david, "p1", "inProgress", new Date(0));
        Sub_Project sp1 = new Sub_Project(1, p1, "s1", "inProgress", new Date(0));
        Asmt a1 = new Asmt(1, david, p1, "testAss");
        LocalDateTime ldt = LocalDateTime.now();


        Comment c1 = new Comment(7, david, "thumbsup", "Test Comment 1", ldt, null, null, null);

        //when
        try {
            Comment saved = commentRepository.save(c1);
            // Then
            fail();
        }
        catch (Exception e){}
    }


    @Test
    public void whenDelete_thenFindAllSizeCorrect(){
        /// given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(1, david, "p1", "inProgress", new Date(0));
        Sub_Project sp1 = new Sub_Project(1, p1, "s1", "inProgress", new Date(0));
        Asmt a1 = new Asmt(1, david, p1, "testAss");
        LocalDateTime ldt = LocalDateTime.now();


        Comment c1 = new Comment(7, david, "thumbsup", "Test Comment 1", ldt, null, null, null);

        entityManager.persist(c1);
        entityManager.flush();

        //when
        commentRepository.delete(c1);
        List<Comment> found = commentRepository.findAll();

        // then
        assertEquals(2, found.size());
    }
}

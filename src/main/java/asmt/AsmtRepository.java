package asmt;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import usr.Usr;

import java.util.List;

@Repository
public interface AsmtRepository extends JpaRepository<Asmt, Long> {
    /**
     *
     * @return Every assignment in our database
     */
    public List<Asmt> findAll();

    /**
     *
     * @param aid the assignment id to search by
     * @return the assignment(if any) that has the given id
     */
    public Asmt findByAid(int aid);

    /** Save a new User
     *
     * @param a the assignment to save
     * @return a - the assignment we saved(useful for testing)
     */
    public Asmt save(Asmt a);

    /** Delete a user
     *
     * @param a - the assigment to delete
     */
    public void delete(Asmt a);
}

package sub_project_roster;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.DemoApplication;

import usr.Usr;
import usr.UsrController;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class Sub_Project_RosterControllerTests {

	// ==== Test the context loading. ==== //

		@Autowired
		private Sub_Project_RosterController cnt;

		@Test
		public void contextLoads() {
			assertNotNull(cnt);
		}

	// ==== Test HTTP requests on dummy data. ==== //

		@LocalServerPort
		private int port;

		@Autowired
		private TestRestTemplate restTemplate;

		// Instance variables for testing
		private String url;
		private String usrId;
		private int subprojectId;
		private Sub_Project_Roster dummy;

		@Before
		public void setup() {
			url = "http://localhost:" + port + "/subprojectRoster/";
			usrId = "wsr";
			subprojectId = 2;
			Sub_Project_RosterKey k = new Sub_Project_RosterKey(1, "ggk");
			dummy = new Sub_Project_Roster();
			dummy.setSubProjectRosterKey(k);
			dummy.setResponsibility("woo");
		}

		@Test
		public void runTestsInOrder() throws JSONException {
			getAllDummyData();
			getDummyData();
			postDummyData();
			putDummyData();
			deleteDummyData();
		}
		
		//@Test
		public void getAllDummyData() throws JSONException {
			// Make an HTTP request using our url.
			String json = this.restTemplate.getForObject(url, String.class);

			// Make sure we got data back.
			assertNotNull(json);

			// Parse the JSON and make sure our array is the correct size.
			JSONArray arr = new JSONArray(json);
			assertEquals(2, arr.length());

			// Make sure the array contains the pairing between subproject 2 and user "wsr".
			assertTrue(json.contains("\"" + "sub_project" + "\":" + subprojectId));
			assertTrue(json.contains("\"" + "student" + "\":\"" + usrId + "\""));
		}

		//@Test
		public void getDummyData() throws JSONException {
			// Make an HTTP request using our url AND our primary key.
			String json = this.restTemplate.getForObject(url + usrId + "/" + subprojectId, String.class);

			// Make sure we got data back.
			assertNotNull(json);
			
			// Parse the JSON and make sure it's the user we're expecting.
			JSONObject ob = new JSONObject(json);
			ob = ob.getJSONObject("subProjectRoster");
			assertEquals(subprojectId, ob.get("sub_project"));
			assertEquals(usrId, ob.get("student"));
		}
		
		//@Test
		public void postDummyData() throws JSONException {
			// Make an HTTP request using our url AND some dummy data.
			String json = this.restTemplate.postForObject(url, dummy, String.class);
			
			// Make sure we got data back.
			assertNotNull(json);
			
			// Parse the JSON and make sure it's the user we're expecting.
			JSONObject ob = new JSONObject(json);			
			ob = ob.getJSONObject("subProjectRoster");
			assertEquals(dummy.getSubProjectRoster().getSub_project(), ob.get("sub_project"));
			assertEquals(dummy.getSubProjectRoster().getStudent(), ob.get("student"));
		}
		
		//@Test
		public void putDummyData() throws JSONException {
			// Modify our dummy data to match an existing primary key.
			Sub_Project_RosterKey existing = new Sub_Project_RosterKey(subprojectId, usrId);
			dummy.setSubProjectRosterKey(existing);
			
			// Make an HTTP request using our url AND some dummy data.
			this.restTemplate.put(url + usrId + "/" + subprojectId, dummy);
			
			// Get by primary key.
			String json = this.restTemplate.getForObject(url + usrId + "/" + subprojectId, String.class);

			// Make sure we got data back.
			assertNotNull(json);
			
			// Parse the JSON and make sure it's the user we're expecting.
			JSONObject ob = new JSONObject(json);
			ob = ob.getJSONObject("subProjectRoster");
			assertEquals(dummy.getSubProjectRoster().getSub_project(), ob.get("sub_project"));
			assertEquals(dummy.getSubProjectRoster().getStudent(), ob.get("student"));
		}
		
		//@Test
		public void deleteDummyData() throws JSONException {
			// Make an HTTP request using our url AND the primary key we want to delete.
			this.restTemplate.delete(url + usrId + "/" + subprojectId);
			
			// Get the object we just deleted by primary key.
			String json = this.restTemplate.getForObject(url + usrId + "/" + subprojectId, String.class);
			
			// It should be null.
			assertNull(json);
		}
}

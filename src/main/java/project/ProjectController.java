package project;

import java.util.*;
import java.sql.Date;
import java.text.SimpleDateFormat;

import org.json.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

import constants.Constants;
import usr.Usr;
import usr.UsrService;

@RestController
public class ProjectController {

	@Autowired
	private ProjectService srv;

	@Autowired
	private UsrService usrv;

	@GetMapping("/projects")
	public @ResponseBody Collection<Project> findAll() {
		return srv.findAll();
	}

	@GetMapping(value = "/projects/{PID}")
	public @ResponseBody Project findByUsername(@PathVariable String PID) {
		try {
			int n = Integer.parseInt(PID);
			Project p = srv.findByPID(n);
			return p;
		} catch (Exception e) {
			return null;
		}
	}

	@GetMapping(value = "/projects/{PID}/owner")
	public @ResponseBody Usr getOwner(@PathVariable String PID) {
		try {
			int n = Integer.parseInt(PID);
			Project p = srv.findByPID(n);
			return p.getUsr();
		} catch (Exception e) {
			return null;
		}
	}

	@PostMapping(value = "/projects")
	public @ResponseBody Project save(@RequestBody String projectJSON) {
		try {
			// Parse the JSON.
			System.out.println(projectJSON);
			JSONObject j = new JSONObject(projectJSON);
			// Populate all the data.
			int pid = (int) (System.currentTimeMillis() / 1000L);
			String owner = j.getString(Constants.OWNER);
			Usr usr = usrv.findByUsername(owner);
			String pname = j.getString(Constants.PNAME);
			String status = j.getString(Constants.STATUS);

			// Parse & process the date.
			String dateString = j.getString(Constants.DUE_DATE);
			SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT);
			java.util.Date dateObject = format.parse(dateString);
			Date due_date = new java.sql.Date(dateObject.getTime());

			Project p = new Project(pid, usr, pname, status, due_date);
			return srv.save(p);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	@PutMapping("/projects/{PID}")
	public @ResponseBody Project updateProject(@PathVariable int PID, @RequestBody String projectJSON ){
		// Get current project info
		Project currentProject = srv.findByPID(PID);
		if(currentProject == null){
			System.out.println("Cant find project");
			return null;
		}

		try {
			// Parse the JSON.
			System.out.println(projectJSON);
			JSONObject j = new JSONObject(projectJSON);
			// Populate all the data.
			int pid = j.getInt(Constants.PID);
			String owner = j.getString(Constants.OWNER);
			Usr usr = usrv.findByUsername(owner);
			if(usr == null){
				System.out.println("Invald User");
				throw new IllegalArgumentException(String.format("Invalid User: User %s doesn't exist",owner));
			}
			String pname = j.getString(Constants.PNAME);
			String status = j.getString(Constants.STATUS);

			// Parse & process the date.
			String dateString = j.getString(Constants.DUE_DATE);
			SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT);
			java.util.Date dateObject = format.parse(dateString);
			Date due_date = new java.sql.Date(dateObject.getTime());

			// Update the values
			currentProject.setUsr(usr);
			currentProject.setPname(pname);
			currentProject.setStatus(status);
			currentProject.setDue_date(due_date);


			srv.save(currentProject);

			return currentProject;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@DeleteMapping(value = "/projects/{PID}")
	public @ResponseBody void delete(@PathVariable int PID) {
		srv.delete(PID);
	}
}

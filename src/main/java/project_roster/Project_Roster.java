package project_roster;

import com.fasterxml.jackson.annotation.JsonIgnore;
import constants.Constants;
import org.json.JSONException;
import org.json.JSONObject;
import project.Project;
import usr.Usr;

import javax.persistence.*;
import java.text.ParseException;

@Entity
public class Project_Roster {
    
    @EmbeddedId
    Project_RosterKey pr;

    @JsonIgnore
    @ManyToOne
    @MapsId("student")
    @JoinColumn(name = "student")
    Usr usr;

    @JsonIgnore
    @ManyToOne
    @MapsId("project")
    @JoinColumn(name = "project")
    Project project;
    
    String responsibility;

    String grade;

    /**
     * No argument constructor
     */
    public Project_Roster() {}



    /**Constructor
     *
     * @param pr
     * @param usr
     * @param project
     * @param responsibility
     */
    public Project_Roster(Project_RosterKey pr, Usr usr, Project project, String responsibility, String grade){
        this.setProjectRosterKey(pr);
        this.setUsr(usr);
        this.setProject(project);
        this.setResponsibility(responsibility);
        this.setGrade(grade);
    }

    /**
     *
     * @return project roster
     */
    public Project_RosterKey getProjectRoster(){
        return this.pr;
    }

    /**
     *
     * @param pr
     */
    public void setProjectRosterKey(Project_RosterKey pr){
        this.pr = pr;
    }

    /**
     *
     * @return usr
     */
    public Usr getUsr(){
        return this.usr;
    }

    /**
     *
     * @param usr
     */
    public void setUsr(Usr usr){
        this.usr = usr;
    }

    /**
     *
     * @return Project
     */
    public Project getProject(){
        return this.project;
    }

    /**
     *
     * @param project
     */
    public void setProject(Project project){
        this.project = project;
    }

    /**
     *
     * @return grade
     */
    public String getResponsibility(){
        return this.responsibility;
    }

    /**
     *
     * @param responsibility
     */
    public void setResponsibility(String responsibility){
        this.responsibility = responsibility;
    }

    /**
     *
     * @param grade - The grade that is to be set.
     */
    public void setGrade(String grade) { this.grade = grade;}

    /**
     *
     * @return The Grade for the project as a string.
     */
    public String getGrade() { return this.grade; }
}

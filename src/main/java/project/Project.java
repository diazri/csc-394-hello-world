package project;

import java.sql.Date;
import java.util.*;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

import asmt.Asmt;
import comment.Comment;
import project_roster.Project_Roster;
import sub_project.Sub_Project;
import usr.Usr;

@Entity
public class Project {

	// Primary key.
	@Id
	private int pID;

	// Foreign key to project owner.
	@ManyToOne
	@JoinColumn(name = "owner", nullable = false)
	@JsonIgnore
	private Usr usr;

	// Foreign key to comments.
	@ManyToMany(  mappedBy = "projects", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
	// @JsonIgnore
	Set<Comment> comments;

	// Delete orphaned subprojects.
	@OneToMany(mappedBy = "project", orphanRemoval = true)
	private Set<Sub_Project> sub_projects;
	
	@OneToMany(mappedBy = "project", orphanRemoval = true)
	private Set<Asmt> assignments;
	
	// Delete orphaned rosters.
	@OneToMany(mappedBy = "project", orphanRemoval = true)
	private Set<Project_Roster> project_rosters;
	
	// Instance variables.
	private String pname;
	private Date due_date;
	private String status;

	/**
	 * A no-argument constructor.
	 */
	public Project() {

	}

	/**
	 * Constructor from constituant elements
	 * @param pid
	 * @param usr
	 * @param pname
	 * @param status
	 * @param due_date
	 */
	public Project(int pid, Usr usr, String pname, String status, Date due_date) {
		this.setPID(pid);
		this.setUsr(usr);
		this.setPname(pname);
		this.setStatus(status);
		this.setDue_date(due_date);
	}

	// Testing a funny new getter.
	public String getOwner() {
		return usr.getUsername();
	}
	
	// Getters and setters.
	/**
	 * @return the pID
	 */
	public int getPID() {
		return pID;
	}

	/**
	 * @param pID the pID to set
	 */
	public void setPID(int pID) {
		this.pID = pID;
	}

	/**
	 * @return the owner
	 */
	public Usr getUsr() {
		return usr;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setUsr(Usr owner) {
		this.usr = owner;
	}

	/**
	 * @return the due_date
	 */
	public Date getDue_date() {
		return due_date;
	}

	/**
	 * @param due_date the due_date to set
	 */
	public void setDue_date(Date due_date) {
		this.due_date = due_date;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the pname
	 */
	public String getPname() {
		return pname;
	}

	/**
	 * @param pname the pname to set
	 */
	public void setPname(String pname) {
		this.pname = pname;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Set<Asmt> getAssignments() {
		return assignments;
	}
}

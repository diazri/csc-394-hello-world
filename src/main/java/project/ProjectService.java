package project;

import java.util.List;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

@Service
public class ProjectService {

	@Autowired
	private ProjectRepository repo;
	
	/** Returns every project in our database.
	 */
	public List<Project> findAll() {
		return repo.findAll();
	}
	
	/** Find a specific project.
	 * 
	 * @param u - the PID of the project we want to find.
	 * @return - the project (if any) that has the given PID.
	 */
	public Project findByPID(int p) {
		return repo.findByPID(p);
	}
	
	/** Save  a project.
	 * 
	 * @param u - the project to save.
	 * @return - the project we saved (useful for testing).
	 */
	public Project save(Project u) {
		return repo.save(u);
	}
	
	/** Delete a project (if it exists).
	 * 
	 * @param p - the project to delete.
	 */
	public void delete(Project p) {
		repo.delete(p);
	}
	
	/** Delete a project with a given pid (if it exists).
	 * 
	 * @param pid - the id of the project to delete.
	 */
	public void delete(int pid) {
		Project p = repo.findByPID(pid);
		delete(p);
	}
}


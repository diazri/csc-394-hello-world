package project_roster;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.Project;
import usr.Usr;

import java.util.List;

@Service
public class Project_RosterService {

	@Autowired
	private Project_RosterRepository repo;

	/**
	 *
	 * @return every project roster entry in our database
	 */
	public List<Project_Roster> findAll() {
		return repo.findAll();
	}

	/**
	 *
	 * @param usr
	 * @return
	 */
	public List<Project_Roster> findAllByUsr(Usr usr) {
		return repo.findAllByUsr(usr);
	}

	/**
	 *
	 * @param project
	 * @return
	 */
	public List<Project_Roster> findAllByProject(Project project) {
		return repo.findAllByProject(project);
	}

	/**
	 *
	 * @param prk
	 * @return
	 */
	public Project_Roster findByPr(Project_RosterKey prk) {
		return repo.findByPr(prk);
	}

	/**
	 *
	 * @param pr
	 * @return
	 */
	public Project_Roster save(Project_Roster pr) {
		return repo.save(pr);
	}

	/**
	 * Delete a roster entry.
	 * 
	 * @param pr - the entry to delete.
	 */
	public void delete(Project_Roster pr) {
		repo.delete(pr);
	}
	
	/** Delete by primary key.
	 * 
	 * @param prk - the key to delete by.
	 */
	public void delete(Project_RosterKey prk) {
		Project_Roster pr = repo.findByPr(prk);
		delete(pr);
	}
}

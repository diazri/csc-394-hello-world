package comment;

import sub_project.Sub_Project;
import usr.Usr;
import project.Project;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import asmt.Asmt;

import java.util.*;
import java.time.LocalDateTime;

@Entity
public class Comment {

	// Primary key.
	@Id
	private int cid;

	// Foreign key to author.
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "author", nullable = false)
	private Usr usr;

	// Foreign keys to project / sub-project / assignment.
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "project_comment", joinColumns = @JoinColumn(name = "comment_id"), inverseJoinColumns = @JoinColumn(name = "project"))
	Set<Project> projects;

	// Foreign keys to project / sub-project / assignment.
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "sub_project_comment", joinColumns = @JoinColumn(name = "comment_id"), inverseJoinColumns = @JoinColumn(name = "sub_project"))
	Set<Sub_Project> sub_projects;

	// Foreign keys to assignment
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "asmt_comment", joinColumns = @JoinColumn(name = "comment_id"), inverseJoinColumns = @JoinColumn(name = "asmt"))
	Set<Asmt> assignments;

	private LocalDateTime time_stamp;
	private String status;
	private String body;

	/**
	 * A no-argument constructor.
	 */
	public Comment() {
		// Do nothing.
	}

	/**
	 * A constructor.
	 *
	 * @param id  - comment id (integer)
	 * @param usr - comment author (in plain text)
	 * @param ldt - timestamp of the comment (in iso format)
	 * @param status  - message associated with the comment (in plain text)
	 * @parmm body - body of the test message (in plan text)
	 * @param pset
	 * @param spset
	 * @param aset
	 */
	public Comment(int id, Usr usr, String status, String body, LocalDateTime ldt,
				   HashSet<Project> pset, HashSet<Sub_Project> spset, HashSet<Asmt> aset) {
		// Store comment id
		this.setCid(id);
		this.setUsr(usr);

		// Store timestamp.
		this.setTimestamp(ldt);

		// Store everything else.
		this.setStatus(status);
		this.setBody(body);

		this.setProjects(pset);
		this.setSub_projects(spset);
		this.setAssignments(aset);
	}

	/**
	 * @return the comment id
	 */
	public int getCid() {
		return cid;
	}

	/**
	 * @param cid the comment id to set
	 */
	public void setCid(int cid) {
		this.cid = cid;
	}

	/**
	 * @return the comment usr
	 */
	/**
	 * Gets the comment Author
	 * 
	 * @return
	 */
	public String getAuthor() {
		return this.usr.getUsername();
	}

	public Usr getUsr() {
		return usr;
	}

	/**
	 * @param usr the comment author to set
	 */

	public void setUsr(Usr usr) {
		this.usr = usr;
	}

	/**
	 * @return the timestamp
	 */
	public LocalDateTime getTimestamp() {
		return time_stamp;
	}

	/**
	 * @param timestamp the email to set
	 */
	public void setTimestamp(LocalDateTime timestamp) {
		this.time_stamp = timestamp;
	}

	/**
	 * @return the comment status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the comment status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the comment body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body the body of the comment
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 *
	 * @return set of projects for a particular comment
	 */
	public Set<Project> getProjects() {
		return projects;
	}

	/**
	 *
	 * @param projects the set of projects that belong to the comment
	 */
	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	/**
	 *
	 * @return Set of sub_projects belonging to a comment
	 */
	public Set<Sub_Project> getSub_projects() {
		return sub_projects;
	}

	/**
	 *
	 * @param sub_projects set of sub_projects that belong to comment to assign
	 */
	public void setSub_projects(Set<Sub_Project> sub_projects) {
		this.sub_projects = sub_projects;
	}

	/**
	 *
	 * @return assignment comment set
	 */

	public Set<Asmt> getAssignments() {
		return assignments;
	}

	/**
	 *
	 * @param assignments set assignment comments set
	 */
	public void setAssignments(Set<Asmt> assignments) {
		this.assignments = assignments;
	}
}
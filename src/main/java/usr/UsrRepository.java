package usr;

import java.util.*;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

@Repository
public interface UsrRepository extends JpaRepository<Usr, Long> {

	/** Returns every user in our database.
	 */
	public List<Usr> findAll();
	
	/** Find a specific user.
	 * 
	 * @param u - the username of the user we want to find.
	 * @return - the user (if any) that has the given username.
	 */
	public Usr findByUsername(String u);
	
	/** Save a new user.
	 * 
	 * @param u - the user to save.
	 * @return u - the user we saved (useful for testing).
	 */
	public Usr save(Usr u);
	
	/** Delete a user.
	 * 
	 * @param u - the user to delete.
	 */

	public void delete(Usr u);
}

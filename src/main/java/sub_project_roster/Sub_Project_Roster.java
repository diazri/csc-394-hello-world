package sub_project_roster;

import com.fasterxml.jackson.annotation.JsonIgnore;
import constants.Constants;
import org.json.JSONException;
import org.json.JSONObject;
import project.Project;
import sub_project.Sub_Project;
import usr.Usr;

import javax.persistence.*;
import java.text.ParseException;

@Entity
public class Sub_Project_Roster {
    
    @EmbeddedId
    Sub_Project_RosterKey spr;

    @JsonIgnore
    @ManyToOne
    @MapsId("student")
    @JoinColumn(name = "student")
    Usr usr;

    @JsonIgnore
    @ManyToOne
    @MapsId("sub_project")
    @JoinColumn(name = "sub_project")
    Sub_Project subProject;

    String responsibility;

    /**
     * No argument constructor
     */
    public Sub_Project_Roster() {}


    /**Constructor
     *
     * @param spr
     * @param usr
     * @param sub_project
     * @param responsibility
     */
    public Sub_Project_Roster(Sub_Project_RosterKey spr, Usr usr, Sub_Project sub_project, String responsibility){
        this.setSubProjectRosterKey(spr);
        this.setUsr(usr);
        this.setSubProject(sub_project);
        this.setResponsibility(responsibility);
    }

    /**
     *
     * @return project roster
     */
    public Sub_Project_RosterKey getSubProjectRoster(){
        return this.spr;
    }

    /**
     *
     * @param spr
     */
    public void setSubProjectRosterKey(Sub_Project_RosterKey spr){
        this.spr = spr;
    }

    /**
     *
     * @return usr
     */
    public Usr getUsr(){
        return this.usr;
    }

    /**
     *
     * @param usr
     */
    public void setUsr(Usr usr){
        this.usr = usr;
    }

    /**
     *
     * @return Sub Project
     */
    public Sub_Project getSubProject(){
        return this.subProject;
    }

    /**
     *
     * @param sub_project
     */
    public void setSubProject(Sub_Project sub_project){
        this.subProject = sub_project;
    }

    /**
     *
     * @return responsibility
     */
    public String getResponsibility(){
        return this.responsibility;
    }

    /**
     *
     * @param responsibility
     */
    public void setResponsibility(String responsibility){
        this.responsibility = responsibility;
    }
}

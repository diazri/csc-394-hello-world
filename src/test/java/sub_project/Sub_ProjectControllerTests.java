package sub_project;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.DemoApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class Sub_ProjectControllerTests {

// ==== Test the context loading. ==== //

	@Autowired
	private Sub_ProjectController cnt;

	@Test
	public void contextLoads() {
		assertNotNull(cnt);
	}

// ==== Test HTTP requests on dummy data. ==== //

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	// Instance variables for testing
	private String url;
	private String primaryKey;
	private int id;
	private Sub_Project dummy;

	@Before
	public void setup() {
		url = "http://localhost:" + port + "/subprojects/";
		primaryKey = "sid";
		id = 1;
		dummy = new Sub_Project();
		dummy.setSid(3);
		dummy.setDue_date(new Date(100000));
		dummy.setSname("new_subproject");
		dummy.setStatus("defined");
	}

	@Test
	public void runTestsInOrder() throws JSONException {
		// getAllDummyData();
		// getDummyData();
		// postDummyData();
		// putDummyData();
		deleteDummyData();
	}

	// @Test
	public void getAllDummyData() throws JSONException {
		// Make an HTTP request using our url.
		String json = this.restTemplate.getForObject(url, String.class);

		// Make sure we got data back.
		assertNotNull(json);

		// Parse the JSON and make sure our array is the correct size.
		JSONArray arr = new JSONArray(json);
		assertEquals(2, arr.length());

		// Make sure the array contains subproject 1.
		assertTrue(json.contains("\"" + primaryKey + "\":\"" + id + "\""));
	}

	// @Test
	public void getDummyData() throws JSONException {
		// Make an HTTP request using our url AND our primary key.
		String json = this.restTemplate.getForObject(url + id, String.class);

		// Make sure we got data back.
		assertNotNull(json);

		// Parse the JSON and make sure it's the subproject we're expecting.
		JSONObject ob = new JSONObject(json);
		assertEquals(id, ob.get(primaryKey));
	}

	// @Test
	public void postDummyData() throws JSONException {
		// Make an HTTP request using our url AND some dummy data.
		String json = this.restTemplate.postForObject(url, dummy, String.class);

		// Make sure we got data back.
		assertNotNull(json);

		// Parse the JSON and make sure it's the subproject we're expecting.
		JSONObject ob = new JSONObject(json);
		assertEquals(dummy.getSid(), ob.get(primaryKey));
		assertEquals(dummy.getStatus(), ob.get("status"));
	}

	// @Test
	public void putDummyData() throws JSONException {
		// Modify our dummy data to match an existing primary key.
		dummy.setSid(id);

		// Make an HTTP request using our url AND some dummy data.
		this.restTemplate.put(url + id, dummy);

		// Get by primary key.
		String json = this.restTemplate.getForObject(url + id, String.class);

		// Make sure we got data back.
		assertNotNull(json);

		// Parse the JSON and make sure it's the subproject we're expecting.
		JSONObject ob = new JSONObject(json);
		assertEquals(dummy.getStatus(), ob.get("status"));
	}

	// @Test
	public void deleteDummyData() throws JSONException {
		// Make an HTTP request using our url AND the primary key we want to delete.
		this.restTemplate.delete(url + id);

		// Get the object we just deleted by primary key.
		String json = this.restTemplate.getForObject(url + id, String.class);

		// It should be null.
		assertNull(json);
	}
}

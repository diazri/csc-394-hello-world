package usr;

import java.util.*;

import org.json.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;
import constants.Constants;

@RestController
public class UsrController {

	@Autowired
	private UsrService srv;
	
	@GetMapping("/users")
	public @ResponseBody Collection<Usr> findAll() {
		return srv.findAll();
	}
	
	@GetMapping(value = "/users/{username}")
	public @ResponseBody Usr findByUsername(@PathVariable String username) {
		Usr u = srv.findByUsername(username);
		return u;
	}
	
	@PostMapping(value = "/users")
	public @ResponseBody Usr save(@RequestBody String usrJSON) {
		try {
			// Instantiate a user and save it.
			Usr u = new Usr(usrJSON);
			return srv.save(u);
		} catch (Exception e) {
			e.printStackTrace();
			// Something went wrong!
			return null;
		}
	}

	@PutMapping("/users/{username}")
	public @ResponseBody Usr updateUser(@PathVariable String username, @RequestBody Usr usr ){
		// Get current user info
		Usr currentUsr = srv.findByUsername(username);
		if(currentUsr == null){
			System.out.println("Cant find user");
			return null;
		}
		// Update the values
		currentUsr.setEmail(usr.getEmail());
		currentUsr.setFirst_name(usr.getFirst_name());
		currentUsr.setLast_name(usr.getLast_name());
		currentUsr.setAuth_level(usr.getAuth_level());

		srv.save(currentUsr);

		return currentUsr;
	}
	
	@DeleteMapping(value = "/users/{username}")
	public @ResponseBody void delete(@PathVariable String username) {
		srv.delete(username);
	}
}

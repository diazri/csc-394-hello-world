package asmt_roster;

import asmt.Asmt;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import project.Project;
import usr.Usr;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(SpringRunner.class)
public class Asmt_RosterServiceIntegrationTests {

    @TestConfiguration
    static class Asmt_RosterServiceImplTestContextConfiguration {

        @Bean
        public Asmt_RosterService asmt_rosterService() {
            return new Asmt_RosterService();
        }
    }

    @Autowired
    private Asmt_RosterService asmt_rosterService;

    @MockBean
    private Asmt_RosterRepository asmt_rosterRepository;

    // write test cases here
    @Before
    public void setUp() {
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(7, david, "p1", "inProgress", new Date(0));
        Asmt a1 = new Asmt(7, david, p1, "Test Assignment");
        Asmt_RosterKey ark = new Asmt_RosterKey(7, "ggk");
        Asmt_Roster ar1 = new Asmt_Roster(ark, david, a1 , "A+");
        Asmt_Roster ar2 = new Asmt_Roster(ark, david, a1 , "B+");
        Asmt_Roster ar3 = new Asmt_Roster(ark, david, a1 , "C+");

        List<Asmt_Roster> lar = new ArrayList<>();
        lar.add(ar1);
        List<Asmt_Roster> larU = new ArrayList<>();
        larU.add(ar2);
        List<Asmt_Roster> larA = new ArrayList<>();
        larA.add(ar3);

        Mockito.when(asmt_rosterRepository.findByAr(any(Asmt_RosterKey.class)))
                .thenReturn(ar1);

        Mockito.when(asmt_rosterRepository.findAllByAsmt(any(Asmt.class)))
                .thenReturn(larA);
        Mockito.when(asmt_rosterRepository.findAllByUsr(any(Usr.class)))
                .thenReturn(larU);

        Mockito.when(asmt_rosterRepository.findAll()).thenReturn(lar);
        Mockito.when(asmt_rosterRepository.save(any(Asmt_Roster.class)))
                .then(returnsFirstArg());


    }

    @Test
    public void whenValidARK_thenAsmtRosterShouldBeFound() {
        Asmt_RosterKey ark1 = new Asmt_RosterKey(7, "ggk");
        Asmt_Roster found = asmt_rosterService.findByAr(ark1);

        assertEquals(ark1.getStudent(), found.getAsmtRoster().getStudent());
        assertEquals(ark1.getAsmt(), found.getAsmtRoster().getAsmt());
    }

    @Test
    public void whenFind_thenReturnCorrectList() {
        List<Asmt_Roster> al = asmt_rosterService.findAll();
        assertEquals(1, al.size());
    }

    @Test
    public void whenFindByUser_thenReturnCorrectList() {
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        List<Asmt_Roster> al = asmt_rosterService.findAllByUsr(david);
        assertEquals(1, al.size());
        assertEquals("B+", al.get(0).getGrade());
    }

    @Test
    public void whenFindAsmt_thenReturnCorrectList() {
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(7, david, "p1", "inProgress", new Date(0));
        Asmt a1 = new Asmt(7, david, p1, "Test Assignment");
        List<Asmt_Roster> al = asmt_rosterService.findAllByAssignment(a1);
        assertEquals(1, al.size());
        assertEquals("C+", al.get(0).getGrade());
    }

    @Test
    public void whenSave_RecieveSameObject() {
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(7, david, "p1", "inProgress", new Date(0));
        Asmt a1 = new Asmt(7, david, p1, "Test Assignment");
        Asmt_RosterKey ark = new Asmt_RosterKey(7, "ggk");
        Asmt_Roster ar1 = new Asmt_Roster(ark, david, a1 , "A+");

        Asmt_Roster found = asmt_rosterService.save(ar1);

        assertEquals("A+", found.getGrade());
    }

    @Test
    public void whenDelete_thenVerify(){
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(7, david, "p1", "inProgress", new Date(0));
        Asmt a1 = new Asmt(7, david, p1, "Test Assignment");
        Asmt_RosterKey ark = new Asmt_RosterKey(7, "ggk");
        Asmt_Roster ar1 = new Asmt_Roster(ark, david, a1 , "A+");

        asmt_rosterService.delete(ar1);

        Mockito.verify(asmt_rosterRepository, Mockito.times(1)).delete(ar1);
    }

    @Test
    public void whenDeleteById_thenVerify(){
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(7, david, "p1", "inProgress", new Date(0));
        Asmt a1 = new Asmt(7, david, p1, "Test Assignment");
        Asmt_RosterKey ark = new Asmt_RosterKey(7, "ggk");
        Asmt_Roster ar1 = new Asmt_Roster(ark, david, a1 , "A+");

        asmt_rosterService.delete(ark);

        Mockito.verify(asmt_rosterRepository, Mockito.times(1)).findByAr(ark);
    }
}

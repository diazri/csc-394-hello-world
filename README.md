# CSC 394 Back-End

This is the back end of our project for CSC 394.

## FAQ

Q: 	How do I launch the application?

A: 	Compile and run DemoApplication.java in src/main/java/com.example.demo. Alternately,
	you can run DemoApplication.java in your IDE of choice.

Q: 	How do I know the application is running?

A: 	Navigate to localhost:8080. If you see the message "Spring is here," then the application is running.

Q:	My POST/PUT/DELETE request isn't working! What's wrong?

A:	Make sure the server is running. Make sure you have the correct URL. Make sure the request body is a valid JSON object, encoded with UTF-8.

Q:  The project/subproject/assignment/comment ID doesn't match the ID that I posted. Why?

A:  These IDs are generated uniquely by the back-end. If you post an ID in the JSON, it will be ignored.

Q: 	What endpoints can I hit?

A: 	As of version 1.0 (hello-world), the available endpoints are:

- /
Displays a "hello-world" message.

- /users
Supports GET all and POST operations.

- /users/{username}
Supports GET by id, PUT, and DELETE operations.

- /projects
Supports GET all and POST operations.

- /projects/{project_id}
Supports GET by id, PUT, and DELETE operations.

- /projects/{project_id}/owner
Supports GET by id operations.

- /projects/{project_id}/subprojects
Supports GET ALL by id operations.

- /projects/{project_id}/assignments
Supports GET ALL by id operations.

- /projectRoster
Supports GET all and POST operations.

- /projectRoster/user/{username}
Supports GET by id operations.

- /projectRoster/projects/{project_id}
Supports GET by id operations.

- /projectRoster/{username}/{project_id}
Supports GET by id, PUT, and DELETE operations.

- /subprojects
Supports GET all and POST operations.

- /subprojects/{subproject_id}
Supports GET by id, PUT, and DELETE operations.

- /subprojectRoster
Supports GET all and POST operations.

- /subprojectRoster/projects/{subproject_id}
Supports GET by id operations.

- /subprojectRoster/user/{username}
Supports GET by id operations.

- /subprojectRoster/{username}/{subproject_id}
Supports GET by id, PUT, and DELETE operations.

- /comments
Supports GET all and POST operations.

- /comments/{comment_id}
Supports GET by id, PUT, and DELETE operations.

- /assignments
Supports GET all and POST operations.

- /assignments/{assignment_id}
Supports GET by id, PUT, and DELETE operations.

- /assignmentRoster
Supports GET all and POST operations.

- /assignmentRoster/assignments/{assignment_id}
Supports GET by id operations.

- /assignmentRoster/user/{username}
Supports GET by id operations.

- /subprojectRoster/{username}/{subproject_id}
Supports GET by id, PUT, and DELETE operations.

### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.
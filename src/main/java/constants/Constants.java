package constants;

public class Constants {
	
	// Our global date format.
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	
	// Constants for getting other JSON objects from JSON.
	public static final String USR = "usr";
	
	// Constants for getting user info from JSON.
	public static final String USERNAME = "username";
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";
	public static final String AUTH_LEVEL = "auth_level";
	public static final String FIRST_NAME = "first_name";
	public static final String LAST_NAME = "last_name";
	
	// Constants for getting project info from JSON.
	public static final String DUE_DATE = "due_date";
	public static final String STATUS = "status";
	public static final String PID = "pid";
	public static final String PNAME = "pname";
	public static final String PARENT = "parent";

	// Constants for getting sub_project info from JSON.
	public static final String SID = "sid";
	public static final String SNAME = "sname";
	public static final String PROJECT = "project";
	
	// Constants for authorization level.
	public static final String USER = "USER";
	public static final String PROFESSOR = "PROFESSOR";
	public static final String ADMIN = "ADMIN";
	
	// Constants for getting assignment info from JSON.
	public static final String AID = "aid";
	public static final String ANAME = "aname";
	public static final String OWNER = "owner";

	//Constants for getting comment info from JSON.
	public static final String CID = "cid";
	public static final String BODY = "body";
	public static final String ASSIGNMENTS = "assignments";
	public static final String SUB_PROJECTS = "sub_projects";
	public static final String PROJECTS = "projects";
	public static final String AUTHOR = "author";
	
	// Constants for getting asmt_roster info from JSON.
	public static final String ASMT = "asmt";
	public static final String GRADE = "grade";
	public static final String ARK = "asmtRoster";
	public static final String STUDENT = "student";

	// Constants for getting project_roster info from JSON
	public static final String RESPONSIBILITY = "responsibility";
	public static final String PRK = "projectRoster";

	// Constant for getting sub_project_roster info from JSON
	public static final String SUB_PROJECT = "sub_project";
	public static final String SPRK = "subProjectRoster";
	
}

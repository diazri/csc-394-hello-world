package comment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import usr.Usr;

import java.util.List;
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    /** Returns every comment in our database.
     */
    public List<Comment> findAll();

    /** Find a specific comment.
     *
     * @param cid - the comment id of the comment we want to find.
     * @return - the comment (if any) that has the given id.
     */
    public Comment findByCid(int cid);

    /** Save a new comment.
     *
     * @param c - the comment to save.
     * @return c - the comment we saved (useful for testing).
     */
    @SuppressWarnings("unchecked")
    public Comment save(Comment c);

    /** Delete a user.
     *
     * @param c - the comment to delete.
     */
    public void delete(Comment c);


}

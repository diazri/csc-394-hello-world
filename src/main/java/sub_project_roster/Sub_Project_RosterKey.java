package sub_project_roster;

import constants.Constants;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Sub_Project_RosterKey implements Serializable {
    @Column(name = "sub_project")
    int sub_project;

    @Column(name = "student")
    String student;

    /**
     * Standard no argument constructor
     */
    public Sub_Project_RosterKey(){}

    /**
     *
     * @param json
     * @throws JSONException
     */
    public Sub_Project_RosterKey(String json) throws JSONException {

        JSONObject j = new JSONObject(json);

        this.setSub_project(j.getInt(Constants.SUB_PROJECT));
        this.setStudent(j.getString(Constants.STUDENT));
    }
    /** Constructor
     *
     * @param sub_project - sub_project id
     * @param student - student username
     */
    public Sub_Project_RosterKey(int sub_project, String student){
        this.setSub_project(sub_project);
        this.setStudent(student);
    }

    /**
     *
     * @return sub project id
     */
    public int getSub_project(){
        return this.sub_project;
    }

    /**
     *
     * @param sub_project
     */
    public void setSub_project(int sub_project){
        this.sub_project = sub_project;
    }

    /**
     *
     * @return student username
     */
    public String getStudent(){
        return this.student;
    }

    /**
     *
     * @param student
     */
    public void setStudent(String student){
        this.student = student;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj == this) return true;
        if(!(obj instanceof Sub_Project_RosterKey)) return false;

        Sub_Project_RosterKey that = (Sub_Project_RosterKey) obj;

        return this.getSub_project()==that.getSub_project() && this.getStudent().equals(that.getStudent());
    }

    @Override
    public int hashCode(){
        int hash = 7;

        hash = 31 * hash + this.getSub_project();
        hash = 31 * hash + (this.getStudent() == null ? 0 : this.getStudent().hashCode());
        return hash;
    }
}

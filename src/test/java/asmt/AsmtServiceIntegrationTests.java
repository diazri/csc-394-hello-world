package asmt;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import project.Project;
import usr.Usr;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(SpringRunner.class)
public class AsmtServiceIntegrationTests {
    @TestConfiguration
    static class AsmtServiceImplTestContextConfiguration {

        @Bean
        public AsmtService asmtService() {
            return new AsmtService();
        }
    }

    @Autowired
    private AsmtService asmtService;

    @MockBean
    private AsmtRepository asmtRepository;

    // write test cases here
    @Before
    public void setUp() {
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(7, david, "p1", "inProgress", new Date(0));
        Asmt a1 = new Asmt(7, david, p1, "Test Assignment");
        List<Asmt> la = new ArrayList<>();
        la.add(a1);

        Mockito.when(asmtRepository.findByAid(anyInt()))
                .thenReturn(a1);

        Mockito.when(asmtRepository.findAll()).thenReturn(la);
        Mockito.when(asmtRepository.save(any(Asmt.class)))
                .then(returnsFirstArg());


    }

    @Test
    public void whenValidAID_thenAsmtShouldBeFound() {
        int aid = 7;
        Asmt found = asmtService.findByAid(7);

        assertEquals(aid, found.getAid());
    }

    @Test
    public void whenFind_thenReturnCorrectList() {
        List<Asmt> al = asmtService.findAll();
        assertEquals(1, al.size());
    }

    @Test
    public void whenSave_RecieveSameObject() {
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p2 = new Project(8, david, "p1", "inProgress", new Date(0));
        Asmt a2 = new Asmt(12, david, p2, "Test2");

        Asmt found = asmtService.save(a2);

        assertEquals(12, found.getAid());
    }

    @Test
    public void whenDelete_thenVerify(){
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p2 = new Project(8, david, "p1", "inProgress", new Date(0));
        Asmt a2 = new Asmt(12, david, p2, "Test2");
        asmtService.delete(a2);

        Mockito.verify(asmtRepository, Mockito.times(1)).delete(a2);
    }

    @Test
    public void whenDeleteById_thenVerify(){
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p2 = new Project(8, david, "p1", "inProgress", new Date(0));
        Asmt a2 = new Asmt(12, david, p2, "Test2");
        asmtService.delete(12);

        Mockito.verify(asmtRepository, Mockito.times(1)).findByAid(12);
    }
}

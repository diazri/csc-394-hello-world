package sub_project;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Set;

import javax.persistence.*;

import comment.Comment;
import constants.Constants;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnore;

import project.Project;
import project_roster.Project_Roster;
import sub_project_roster.Sub_Project_Roster;
import usr.Usr;

@Entity
public class Sub_Project {

	// Primary key.
	@Id
	private int sid;
	
	// Foreign key to parent project.
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "parent", nullable = false)
	private Project project;
	
	// Foreign key to comments.
	@ManyToMany(mappedBy = "sub_projects", fetch = FetchType.LAZY, cascade = CascadeType.ALL )
	// @JsonIgnore
	Set<Comment> comments;

	// Auto-remove orphaned roster entries.
	@OneToMany(mappedBy = "subProject", orphanRemoval = true)
	private Set<Sub_Project_Roster> sub_project_rosters;
	
	// Instance variables.
	private String sname;
	private Date due_date;
	private String status;

    /**
     * Simply no argument constuctor
     */
	public Sub_Project(){

    }


    public Sub_Project(int sid, Project parent, String sname, String status, Date due_date) {
       this.setSid(sid);
       this.setProject(parent);
       this.setSname(sname);
       this.setStatus(status);
       this.setDue_date(due_date);
    }

	// Getters and setters (auto-generated).
	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public Date getDue_date() {
		return due_date;
	}

	public void setDue_date(Date due_date) {
		this.due_date = due_date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}
	
	/**
	 * @return the sID
	 */
	public int getSid() {
		return sid;
	}

	/**
	 * @param sID the sID to set
	 */
	public void setSid(int sID) {
		this.sid = sID;
	}

	/**
	 * @return the project
	 */
	public Project getProject() {
		return project;
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(Project project) {
		this.project = project;
	}

	/**
	 *
	 * @return
	 */
	public int getParent() {
		return this.project.getPID();
	}
	
	
}

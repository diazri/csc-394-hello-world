package comment;

import asmt.Asmt;
import com.example.demo.DemoApplication;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.test.context.junit4.SpringRunner;
import project.Project;
import project.ProjectController;
import sub_project.Sub_Project;
import usr.Usr;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.HashSet;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = DemoApplication.class)

public class CommentControllerIntegrationTests {
    // ==== Test the context loading. ==== //

    @Autowired
    private CommentController ccnt;

    @Autowired
    private ProjectController pcnt;

    @Test
    public void contextLoads() {
        assertNotNull(ccnt);
    }

    // ==== Test HTTP requests on dummy data. ==== //

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    // Instance variables for testing
    private String url;
    private String primaryKey;
    private int id;
    private Comment dummy;
    private HttpHeaders headers;
    private JSONObject commentJsonObject;
    private JSONObject projectJsonObject;
    private JSONArray projectJsonArray;
    private JSONObject subpJsonObject;
    private JSONArray subpJsonArray;
    private JSONObject asmtJsonObject;
    private JSONArray asmtJsonArray;

    @Before
    public void setup() throws JSONException {
        url = "http://localhost:" + port + "/comments/";
        primaryKey = "cid";
        id = 1;
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        commentJsonObject = new JSONObject();
        commentJsonObject.put("cid",7);
        commentJsonObject.put("author","ggk");
        commentJsonObject.put("status", "thumbs up");
        commentJsonObject.put("body","Test Comment");

        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        dummy = new Comment(7, david, "thumbs up", "Test Comment", LocalDateTime.now(),
                            null, null, null);
    }

    @Test
    public void runProjectTestsInOrder() throws JSONException {
        getAllCommentData();
        getCommentByIdData();
        postDummyCommentData();
        postCommentWithProject();
        postCommentWithSubProject();
        postCommentWithAssignment();
        putDummyCommentData();
        deleteDummyCommentData();
    }

    //@Test
    public void getAllCommentData() throws JSONException {
        // Make an HTTP request using our url.
        String json = this.restTemplate.getForObject(url, String.class);
        //System.out.println(json);

        // Make sure we got data back.
        assertNotNull(json);

        // Parse the JSON and make sure our array is the correct size.
        JSONArray arr = new JSONArray(json);
        assertEquals(2, arr.length());

        // Make sure the array contains the project with id 1.
        assertTrue(json.contains("\"" + primaryKey + "\":" + id));
    }

    //@Test
    public void getCommentByIdData() throws JSONException {
        // Make an HTTP request using our url AND our primary key.
        String json = this.restTemplate.getForObject(url + id, String.class);

        // Make sure we got data back.
        assertNotNull(json);

        // Parse the JSON and make sure it's the user we're expecting.
        JSONObject ob = new JSONObject(json);
        assertEquals(id, ob.get(primaryKey));
    }

    //@Test
    public void postDummyCommentData() throws JSONException {
        // Make an HTTP request using our url AND some dummy data.
        HttpEntity<String> request = new HttpEntity<>(commentJsonObject.toString(), headers);
        String json = this.restTemplate.postForObject(url, request, String.class);

        // Make sure we got data back.
        assertNotNull(json);

        // Parse the JSON and make sure it's the user we're expecting.
        JSONObject ob = new JSONObject(json);
        assertEquals(dummy.getCid(), ob.get(primaryKey));
        assertEquals(dummy.getAuthor(),ob.get("author"));
    }

    //@Test
    public void postCommentWithProject() throws JSONException {
        // Post a comment attached to a Project.
        projectJsonArray = new JSONArray();
        projectJsonObject = new JSONObject();
        projectJsonObject.put("pid", 1);
        projectJsonArray.put(projectJsonObject);
        commentJsonObject.put("projects", projectJsonArray);
        HttpEntity<String> request = new HttpEntity<>(commentJsonObject.toString(), headers);
        this.restTemplate.postForObject(url, request, String.class);

        // Make sure the project actually contains the comment
        String json = this.restTemplate.getForObject("/projects/1", String.class);
        // System.out.println(json);

        // Make sure we got data back.
        assertNotNull(json);

        // Parse the JSON and make sure it's the user we're expecting.
        JSONObject ob = new JSONObject(json);
        JSONArray pArray = ob.getJSONArray("comments");
        assertEquals(pArray.length(), 2);

        assertTrue(json.contains("\"" + primaryKey + "\":" + id));

    }

    //@Test
    public void postCommentWithSubProject() throws JSONException {
        // Post a comment attached to a Project.
        subpJsonArray = new JSONArray();
        subpJsonObject = new JSONObject();
        subpJsonObject.put("sid", 1);
        subpJsonArray.put(subpJsonObject);
        commentJsonObject.put("sub_projects", subpJsonArray);
        HttpEntity<String> request = new HttpEntity<>(commentJsonObject.toString(), headers);
        this.restTemplate.postForObject(url, request, String.class);

        // Make sure the project actually contains the comment
        String json = this.restTemplate.getForObject("/subprojects/1", String.class);
        // System.out.println(json);

        // Make sure we got data back.
        assertNotNull(json);

        // Parse the JSON and make sure it's the user we're expecting.
        JSONObject ob = new JSONObject(json);
        JSONArray pArray = ob.getJSONArray("comments");
        assertEquals(pArray.length(), 2);

        assertTrue(json.contains("\"" + primaryKey + "\":" + id));

    }

    //@Test
    public void postCommentWithAssignment() throws JSONException {
        // Post a comment attached to a Project.
        asmtJsonArray= new JSONArray();
        asmtJsonObject= new JSONObject();
        asmtJsonObject.put("aid", 1);
        asmtJsonArray.put(asmtJsonObject);
        commentJsonObject.put("assignments", asmtJsonArray);
        HttpEntity<String> request = new HttpEntity<>(commentJsonObject.toString(), headers);
        this.restTemplate.postForObject(url, request, String.class);

        // Make sure the project actually contains the comment
        String json = this.restTemplate.getForObject("/assignments/1", String.class);
        // System.out.println(json);

        // Make sure we got data back.
        assertNotNull(json);

        // Parse the JSON and make sure it's the user we're expecting.
        JSONObject ob = new JSONObject(json);
        JSONArray pArray = ob.getJSONArray("comments");
        assertEquals(pArray.length(), 2);

        assertTrue(json.contains("\"" + primaryKey + "\":" + id));

    }

    //@Test
    public void putDummyCommentData() throws JSONException {
        // Modify our dummy data to match an existing primary key.
        dummy.setCid(id);

        // Make an HTTP request using our url AND some dummy data.
        this.restTemplate.put(url + id, dummy);

        // Get by primary key.
        String json = this.restTemplate.getForObject(url + id, String.class);

        // Make sure we got data back.
        assertNotNull(json);

        // Parse the JSON and make sure it's the user we're expecting.
        JSONObject ob = new JSONObject(json);
        assertEquals(dummy.getBody(), ob.get("body"));
    }

    //@Test
    public void deleteDummyCommentData() throws JSONException {
        // Make an HTTP request using our url AND the primary key we want to delete.
        this.restTemplate.delete(url + id);

        // Get the object we just deleted by primary key.
        String json = this.restTemplate.getForObject(url + id, String.class);

        // It should be null.
        assertNull(json);
    }

}

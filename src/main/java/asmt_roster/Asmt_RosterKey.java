package asmt_roster;

import constants.Constants;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Asmt_RosterKey implements Serializable {
    @Column(name="asmt")
    int asmt;

    @Column(name="student")
    String student;

    /**
     * Standard no argument constructor
     */
    public Asmt_RosterKey(){}

    /** Constructor
     *
     * @param asmt - comment id
     * @param student - student username
     */
    public Asmt_RosterKey(int asmt, String student){
        this.setAsmt(asmt);
        this.setStudent(student);
    }

    public Asmt_RosterKey(String json) throws JSONException {

        JSONObject j = new JSONObject(json);

        this.setAsmt(j.getInt(Constants.ASMT));
        this.setStudent(j.getString(Constants.STUDENT));
    }

    /**
     *
     * @return asmt id
     */
    public int getAsmt(){
        return this.asmt;
    }

    /**
     *
     * @param asmt
     */
    public void setAsmt(int asmt){
        this.asmt = asmt;
    }

    /**
     *
     * @return student username
     */
    public String getStudent(){
        return this.student;
    }

    /**
     *
     * @param student
     */
    public void setStudent(String student){
        this.student = student;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj == this) return true;
        if(!(obj instanceof Asmt_RosterKey)) return false;

        Asmt_RosterKey that = (Asmt_RosterKey) obj;

        return this.getAsmt()==that.getAsmt() && this.getStudent().equals(that.getStudent());
    }

    @Override
    public int hashCode(){
        int hash = 7;

        hash = 31 * hash + this.getAsmt();
        hash = 31 * hash + (this.getStudent() == null ? 0 : this.getStudent().hashCode());
        return hash;
    }
}

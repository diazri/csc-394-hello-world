package sub_project_roster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sub_project.Sub_Project;
import usr.Usr;

import java.util.List;

@Repository
public interface Sub_Project_RosterRepository extends JpaRepository<Sub_Project_Roster, Long> {
    /**
     *
     * @return Every Sub Project/Student pairing in our database
     */
    public List<Sub_Project_Roster> findAll();

    /**
     *
     * @param usr - user to find all sub_project rosters
     * @return
     */
    public List<Sub_Project_Roster> findAllByUsr(Usr usr);

    /**
     *
     * @param sub_project
     * @return
     */
    public List<Sub_Project_Roster> findAllBySubProject(Sub_Project sub_project);

    /**
     *
     * @param sprk
     * @return
     */
    public Sub_Project_Roster findBySpr(Sub_Project_RosterKey sprk);


    /** Save a new Sub Project Roster entry
     *
     * @param spr the sub project roster entry to save
     * @return pr - the sub project roster entry we saved(useful for testing)
     */
    public Sub_Project_Roster save(Sub_Project_Roster spr);

    /** Delete a sub project roster entry
     *
     * @param spr- the sub project roster entry to delete
     */
    public void delete(Sub_Project_Roster spr);
}

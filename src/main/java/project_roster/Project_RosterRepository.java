package project_roster;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.Project;
import usr.Usr;

import java.util.List;

@Repository
public interface Project_RosterRepository extends JpaRepository<Project_Roster, Project_RosterKey> {
    /**
     *
     * @return Every Project/Student pairing in our database
     */
    public List<Project_Roster> findAll();

    /**
     *
     * @param usr - user to find all project rosters
     * @return
     */
    public List<Project_Roster> findAllByUsr(Usr usr);

    /**
     *
     * @param project project to find all project rosters by
     * @return
     */
    public List<Project_Roster> findAllByProject(Project project);

    /**
     *
     * @param prk
     * @return
     */
    public Project_Roster findByPr(Project_RosterKey prk);

    /** Save a new Project_Roster Responsibility
     *
     * @param pr the project roster entry to save
     * @return pr - the project roster entry we saved(useful for testing)
     */
    public Project_Roster save(Project_Roster pr);

    /** Delete a roster entry.
     *
     * @param pr- the project roster entry to delete
     */
    public void delete(Project_Roster pr);
}

package sub_project;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.Project;

@Service
public class Sub_ProjectService {

	@Autowired
	private Sub_ProjectRepository repo;
	
	/** Returns every subproject in our database.
	 */
	public List<Sub_Project> findAll() {
		return repo.findAll();
	}
	
	/** Find a specific subproject.
	 * 
	 * @param p - the SID of the subproject we want to find.
	 * @return - the subproject (if any) that has the given SID.
	 */
	public Sub_Project findBySid(int p) {
		return repo.findBySid(p);
	}

	/**
	 *
	 * @param p - the Project for which we want all subproject
	 * @return - The list of subprojects
	 */
	public List<Sub_Project> findAllByProject(Project p) { return repo.findAllByProject(p);}

	/** Save  a sub_project.
	 *
	 * @param u - the project to save.
	 * @return - the project we saved (useful for testing).
	 */
	public Sub_Project save(Sub_Project u) {
		return repo.save(u);
	}
	
	/** Delete a given subproject (if it exists)
	 * 
	 * @param s - the subproject to delete.
	 */
	public void delete(Sub_Project s) {
		repo.delete(s);
	}
	
	/** Delete by primary key.
	 * 
	 * @param p - the subproject id to delete.
	 */
	public void delete(int p) {
		Sub_Project s = findBySid(p);
		repo.delete(s);
	}
}

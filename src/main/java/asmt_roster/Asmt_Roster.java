package asmt_roster;

import asmt.AsmtService;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;
import project.ProjectService;
import usr.Usr;

import java.text.ParseException;

import javax.persistence.*;

import org.json.JSONException;
import org.json.JSONObject;

import asmt.Asmt;
import constants.Constants;
import usr.UsrService;


@Entity
public class Asmt_Roster {


    @EmbeddedId
    Asmt_RosterKey ar;

    @JsonIgnore
    @ManyToOne
    @MapsId("student")
    @JoinColumn(name = "student")
    Usr usr;

    @JsonIgnore
    @ManyToOne
    @MapsId("asmt")
    @JoinColumn(name = "asmt")
    Asmt asmt;

    String grade;


    /**
     * No argument constructor
     */
    public Asmt_Roster() {}


    /**Constructor
     *
     * @param ar
     * @param usr
     * @param asmt
     * @param grade
     */
    public Asmt_Roster(Asmt_RosterKey ar, Usr usr, Asmt asmt, String grade){
        this.setAsmtRosterKey(ar);
        this.setUsr(usr);
        this.setAsmt(asmt);
        this.setGrade(grade);
    }

    /**
     *
     * @return asmt roster
     */
    public Asmt_RosterKey getAsmtRoster(){
        return this.ar;
    }

    /**
     *
     * @param ar
     */
    public void setAsmtRosterKey(Asmt_RosterKey ar){
        this.ar = ar;
    }

    /**
     *
     * @return usr
     */
    public Usr getUsr(){
        return this.usr;
    }

    /**
     *
     * @param usr
     */
    public void setUsr(Usr usr){
        this.usr = usr;
    }



    /**
     *
     * @return Asmt
     */
    public Asmt getAsmt(){
        return this.asmt;
    }

    /**
     *
     * @param asmt
     */
    public void setAsmt(Asmt asmt){
        this.asmt = asmt;
    }



    /**
     *
     * @return grade
     */
    public String getGrade(){
        return this.grade;
    }

    /**
     *
     * @param grade
     */
    public void setGrade(String grade){
        this.grade = grade;
    }
}

package project;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import usr.Usr;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(SpringRunner.class)
public class ProjectServiceIntegrationTests {

    @TestConfiguration
    static class ProjectServiceImplTestContextConfiguration {

        @Bean
        public ProjectService projectService() {
            return new ProjectService();
        }
    }

    @Autowired
    private ProjectService projectService;

    @MockBean
    private ProjectRepository projectRepository;

    // write test cases here
    @Before
    public void setUp() {
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(7, david, "p1", "inProgress", new Date(0));
        List<Project> lp = new ArrayList<>();
        lp.add(p1);

        Mockito.when(projectRepository.findByPID(anyInt()))
                .thenReturn(p1);

        Mockito.when(projectRepository.findAll()).thenReturn(lp);
        Mockito.when(projectRepository.save(any(Project.class)))
                .then(returnsFirstArg());


    }

    @Test
    public void whenValidPID_thenProjectShouldBeFound() {
        int pid = 7;
        Project found = projectService.findByPID(7);

        assertEquals(pid, found.getPID());
    }

    @Test
    public void whenFind_thenReturnCorrectList() {
        List<Project> pl = projectService.findAll();
        assertEquals(1, pl.size());
    }

    @Test
    public void whenSave_RecieveSameObject() {
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p2 = new Project(8, david, "p1", "inProgress", new Date(0));

        Project found = projectService.save(p2);

        assertEquals(8, found.getPID());
    }

    @Test
    public void whenDelete_thenVerify(){
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p2 = new Project(8, david, "p1", "inProgress", new Date(0));

        projectService.delete(p2);

        Mockito.verify(projectRepository, Mockito.times(1)).delete(p2);
    }

    @Test
    public void whenDeleteById_thenVerify(){
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p2 = new Project(8, david, "p1", "inProgress", new Date(0));

        projectService.delete(8);

        Mockito.verify(projectRepository, Mockito.times(1)).findByPID(8);
    }

}

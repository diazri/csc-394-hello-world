package asmt;

import java.util.List;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

@Service
public class AsmtService {

    @Autowired
    private AsmtRepository repo;

    /**
     *
     * @return every assignment in our database
     */
    public List<Asmt> findAll(){ return repo.findAll();}

    /**
     *
     * @param a - the assignment id of the assignemnt we want to find
     * @return - the assignment (if any) that has the given id
     */
    public Asmt findByAid(int a) { return repo.findByAid(a);}

    /** Save an assignment
     *
     * @param a - the assignment to save
     * @return - the assignment we saved (useful for testing)
     */
    public Asmt save(Asmt a) { return repo.save(a);}
    
    /** Delete an assignment.
     * 
     * @param a - the assignment to delete.
     */
    public void delete(Asmt a) {
    	repo.delete(a);
    }
    
    /** Delete by primary key.
     * 
     * @param aid - the primary key of the assignment to delete.
     */
    public void delete(int aid) {
    	Asmt a = findByAid(aid);
    	delete(a);
    }
}

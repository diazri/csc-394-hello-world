package sub_project;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.Project;

@Repository
public interface Sub_ProjectRepository extends JpaRepository<Sub_Project, Long> {

	/** Returns every subproject in our database.
	 */
	public List<Sub_Project> findAll();
	
	/** Find a specific subproject (by PID).
	 * 
	 * @param s - the id of the subproject we want to find.
	 * @return - the subproject (if any) that has the given SID.
	 */
	public Sub_Project findBySid(int s);

	/**
	 *
	 * @param p the project to find all subprojects by
	 * @return - The list of subprojects belonging to the given project
	 */
	public List<Sub_Project> findAllByProject(Project p);
	
	/** Delete a given subproject (if it exists).
	 * 
	 * @ param s - the subproject to delete.
	 */
	public void delete(Sub_Project s);
}

package asmt_roster;

import asmt.Asmt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import usr.Usr;

import java.util.List;

@Service
public class Asmt_RosterService {

    @Autowired
    private Asmt_RosterRepository repo;

    /**
     *
     * @return every assignment grade in our database
     */
    public List<Asmt_Roster> findAll(){
        return repo.findAll();
    }

    /**
     *
     * @param usr
     * @return
     */
    public List<Asmt_Roster> findAllByUsr(Usr usr) {return repo.findAllByUsr(usr);}

    /**
     *
     * @param asmt
     * @return
     */
    public List<Asmt_Roster> findAllByAssignment(Asmt asmt) {return repo.findAllByAsmt(asmt);}

    /**
     * 
     * @param ark
     * @return
     */
    public Asmt_Roster findByAr(Asmt_RosterKey ark) {return repo.findByAr(ark);}

    /**
     *
     * @param g
     * @return
     */
    public Asmt_Roster save(Asmt_Roster g) {return repo.save(g);}
    
    /** Delete asmt_roster.
     * 
     * @param a - the roster entry to delete.
     */
    public void delete(Asmt_Roster a) {
    	repo.delete(a);
    }
    
    /** Delete by primary key.
     * 
     * @param ark - the key to delete by.
     */
    public void delete(Asmt_RosterKey ark) {
    	Asmt_Roster a = repo.findByAr(ark);
    	delete(a);
    }
}

package usr;

import java.util.Collection;
import java.util.Set;

import javax.persistence.*;

import org.json.JSONObject;

import asmt_roster.Asmt_Roster;
import comment.Comment;
import project_roster.Project_Roster;
import sub_project_roster.Sub_Project_Roster;
import constants.Constants;

@Entity
public class Usr {

	@Id
	private String username;
	private String email;
	private String password; // Should be hashed, not stored raw.
	private String first_name;
	private String last_name;
	private String auth_level;

	// For deleting orphaned comments.
	@OneToMany(mappedBy = "usr", orphanRemoval = true)
    private Set<Comment> comments;
	
	// For deleting orphaned roster entries.
	@OneToMany(mappedBy = "usr", orphanRemoval = true)
	private Set<Project_Roster> project_rosters;
	
	@OneToMany(mappedBy = "usr", orphanRemoval = true)
	private Set<Sub_Project_Roster> subproject_rosters;
	
	@OneToMany(mappedBy = "usr", orphanRemoval = true)
	private Set<Asmt_Roster> asmt_rosters;
	
	/**
	 * A no-argument constructor.
	 */
	public Usr() {
		// Do nothing.
	}

	/**
	 * A constructor for handling JSON.
	 * 
	 * @param json - the JSON string to use.
	 */
	public Usr(String json) {
		// Parse the JSON.
		// System.out.println(json);
		JSONObject j = new JSONObject(json);
		
		// Populate all the data.
		this.setUsername( 	j.getString(Constants.USERNAME));
		this.setEmail( 		j.getString(Constants.EMAIL));
		this.setPassword( 	j.getString(Constants.PASSWORD));
		this.setFirst_name( j.getString(Constants.FIRST_NAME));
		this.setLast_name( 	j.getString(Constants.LAST_NAME));
		this.setAuth_level( j.getString(Constants.AUTH_LEVEL));
	}

	/**
	 * A constructor.
	 * 
	 * @param u - the username (in plain text)
	 * @param e - the user's e-mail (in plain text)
	 * @param p - the user's password (already hashed)
	 */
	public Usr(String u, String e, String p, String f, String l, String a) {
		// Store username & e-mail raw.
		this.setUsername(u);
		this.setEmail(e);

		// Store pre-hashed password.
		this.setPassword(p);

		// Store everything else.
		this.setFirst_name(f);
		this.setLast_name(l);
		this.setAuth_level(a);
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the (hashed) password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the (hashed) password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the first_name
	 */
	public String getFirst_name() {
		return first_name;
	}

	/**
	 * @param first_name the first_name to set
	 */
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	/**
	 * @return the last_name
	 */
	public String getLast_name() {
		return last_name;
	}

	/**
	 * @param last_name the last_name to set
	 */
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	/**
	 * @return the auth_level
	 */
	public String getAuth_level() {
		return auth_level;
	}

	/**
	 * @param auth_level the auth_level to set
	 */
	public void setAuth_level(String auth_level) {
		this.auth_level = auth_level;
	}

}
package project;

import com.example.demo.DemoApplication;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import usr.Usr;

import java.sql.Date;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = DemoApplication.class)

public class ProjectControllerIntegrationTests {
    // ==== Test the context loading. ==== //

    @Autowired
    private ProjectController pcnt;

    @Test
    public void contextLoads() {
        assertNotNull(pcnt);
    }

// ==== Test HTTP requests on dummy data. ==== //

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    // Instance variables for testing
    private String url;
    private String primaryKey;
    private int id;
    private Project dummy;

    @Before
    public void setup() {
        url = "http://localhost:" + port + "/projects/";
        primaryKey = "pid";
        id = 1;
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        dummy = new Project(7, david, "p1", "inProgress", new Date(0));
    }

    @Test
    public void runProjectTestsInOrder() throws JSONException {
        getAllProjectData();
        getProjectByIdData();
        postDummyProjectData();
        putDummyProjectData();
        deleteDummyProjectData();
    }

    //@Test
    public void getAllProjectData() throws JSONException {
        // Make an HTTP request using our url.
        String json = this.restTemplate.getForObject(url, String.class);
        System.out.println(json);

        // Make sure we got data back.
        assertNotNull(json);

        // Parse the JSON and make sure our array is the correct size.
        JSONArray arr = new JSONArray(json);
        assertEquals(2, arr.length());

        // Make sure the array contains the project with id 1.
        assertTrue(json.contains("\"" + primaryKey + "\":" + id));

    }

    //@Test
    public void getProjectByIdData() throws JSONException {
        // Make an HTTP request using our url AND our primary key.
        String json = this.restTemplate.getForObject(url + id, String.class);

        // Make sure we got data back.
        assertNotNull(json);

        // Parse the JSON and make sure it's the user we're expecting.
        JSONObject ob = new JSONObject(json);
        assertEquals(id, ob.get(primaryKey));

        // Make sure the right comment is attached and exists.
        JSONArray cArray = ob.getJSONArray("comments");
        assertEquals(1, cArray.length());
        assertTrue(json.contains("\"" + "cid" + "\":" + 1));
    }

    //@Test
    public void postDummyProjectData() throws JSONException {
        // Make an HTTP request using our url AND some dummy data.
        String json = this.restTemplate.postForObject(url, dummy, String.class);

        // Make sure we got data back.
        assertNotNull(json);

        // Parse the JSON and make sure it's the user we're expecting.
        JSONObject ob = new JSONObject(json);
        assertEquals(dummy.getPID(), ob.get(primaryKey));
        assertEquals(dummy.getOwner(),ob.get("owner"));
    }

    //@Test
    public void putDummyProjectData() throws JSONException {
        // Modify our dummy data to match an existing primary key.
        dummy.setPID(id);

        // Make an HTTP request using our url AND some dummy data.
        this.restTemplate.put(url + id, dummy);

        // Get by primary key.
        String json = this.restTemplate.getForObject(url + id, String.class);

        // Make sure we got data back.
        assertNotNull(json);

        // Parse the JSON and make sure it's the user we're expecting.
        JSONObject ob = new JSONObject(json);
        assertEquals(dummy.getPname(), ob.get("pname"));
    }

    //@Test
    public void deleteDummyProjectData() throws JSONException {
        // Make an HTTP request using our url AND the primary key we want to delete.
        this.restTemplate.delete(url + id);

        // Get the object we just deleted by primary key.
        String json = this.restTemplate.getForObject(url + id, String.class);

        // It should be null.
        assertNull(json);
    }

}

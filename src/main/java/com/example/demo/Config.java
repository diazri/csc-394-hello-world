package com.example.demo;

import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.*;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@ComponentScan({"usr", "project", "sub_project", "comment","asmt","asmt_roster", "project_roster",
                "sub_project_roster"})
@EnableJpaRepositories(basePackages = {"usr", "project", "sub_project", "comment","asmt","asmt_roster",
                                        "project_roster", "sub_project_roster"})
public class Config extends WebMvcConfigurerAdapter {
	
	@Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("POST", "PUT", "GET", "DELETE");
    }
}

package comment;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import asmt.Asmt;
import asmt.AsmtService;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.json.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;
import constants.Constants;
import project.Project;
import project.ProjectService;
import sub_project.Sub_Project;
import sub_project.Sub_ProjectService;
import usr.Usr;
import usr.UsrService;

@RestController
public class CommentController {

    @Autowired
    private CommentService srv;
    
    @Autowired
    private UsrService usrv;

    @Autowired
    private ProjectService psrv;

    @Autowired
    private Sub_ProjectService ssrv;

    @Autowired
    private AsmtService asrv;

    @GetMapping("/comments")
    public @ResponseBody Collection<Comment> findAll() {
        return srv.findAll();
    }

    @GetMapping(value = "/comments/{cid}")
    public @ResponseBody Comment findByUsername(@PathVariable int cid) {
        Comment c = srv.findByCid(cid);
        return c;
    }

    @PostMapping(value = "/comments")
    public @ResponseBody Comment save(@RequestBody String commentJSON) {
        try {
            // Parse the JSON.
            //System.out.println(commentJSON);
            JSONObject j = new JSONObject(commentJSON);

            // Populate all the data.
            int cid = (int) (System.currentTimeMillis() / 1000L);
            String author = j.getString(Constants.AUTHOR);
            Usr usr = usrv.findByUsername(author);
            String status = j.getString(Constants.STATUS);
            String body = j.getString(Constants.BODY);

            // Instantiate empty sets.
            HashSet<Asmt> AsmtSet =new HashSet<>();
            HashSet<Project> ProjectSet =new HashSet<>();
            HashSet<Sub_Project> Sub_ProjectSet =new HashSet<>();


            // Add any projects to our set.
            try {
                JSONArray jr = j.getJSONArray(Constants.PROJECTS);
                for (int i = 0; i < jr.length(); i++) {
                    int pid = jr.getJSONObject(i).getInt(Constants.PID);
                    ProjectSet.add(psrv.findByPID(pid));
                }
                System.out.println(ProjectSet.size());
            } catch (org.json.JSONException e) {
                e.printStackTrace();
            }

            // Add any assignments to our set.
            try {
                JSONArray jr = j.getJSONArray(Constants.ASSIGNMENTS);
                for (int i = 0; i < jr.length(); i++) {
                    int aid = jr.getJSONObject(i).getInt(Constants.AID);
                    AsmtSet.add(asrv.findByAid(aid));
                }
                System.out.println(ProjectSet.size());

            } catch (org.json.JSONException e) {
                e.printStackTrace();
            }

            // Add any subprojects to our set.
            try {
                JSONArray jr = j.getJSONArray(Constants.SUB_PROJECTS);
                for (int i = 0; i < jr.length(); i++) {
                    int sid = jr.getJSONObject(i).getInt(Constants.SID);
                    Sub_ProjectSet.add(ssrv.findBySid(sid));
                }
                System.out.println(ProjectSet.size());
            } catch (org.json.JSONException e) {
                e.printStackTrace();
            }

            // Parse & process the date.
            LocalDateTime now = LocalDateTime.now();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


            // Instantiate a project and save it.
            Comment p = new Comment(cid, usr, status, body, now, ProjectSet, Sub_ProjectSet, AsmtSet);
            return srv.save(p);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PutMapping(value = "/comments/{cid}")
    public @ResponseBody Comment updateComment(@PathVariable int cid, @RequestBody String commentJSON){
        // Get current project info
        // System.out.println(commentJSON);
        Comment currentComment = srv.findByCid(cid);
        if(currentComment == null){
            System.out.println("Cant find Comment");
            return null;
        }

        try {
            // Parse the JSON.
            JSONObject j = new JSONObject(commentJSON);

            // Populate all the data.
            String author = j.getString(Constants.AUTHOR);
            Usr usr = usrv.findByUsername(author);
            if(usr == null){
                System.out.println("User doesn't exist");
                return null;
            }
            String status = j.getString(Constants.STATUS);
            String body = j.getString(Constants.BODY);


            // Parse & process the date.
            LocalDateTime now = LocalDateTime.now();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            // Update the values
            currentComment.setUsr(usr);
            currentComment.setStatus(status);
            currentComment.setBody(body);

            // Update timestamp?
            // currentComment.setTimestamp(now);
            srv.save(currentComment);
            return currentComment;

        } catch (Exception e) {
            e.printStackTrace();
            //return new Comment(-1, null, e.getMessage(), e.toString(), null, null, null, null);
            return null;
        }


    }

    @DeleteMapping(value = "/comments/{cid}")
    public @ResponseBody void delete(@PathVariable int cid) {
        srv.delete(cid);
    }

}

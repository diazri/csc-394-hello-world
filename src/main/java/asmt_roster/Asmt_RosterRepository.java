package asmt_roster;

import asmt.Asmt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import usr.Usr;

import java.util.List;

@Repository
public interface Asmt_RosterRepository extends JpaRepository<Asmt_Roster, Long> {
    /**
     *
     * @return Every graded assignment in our database
     */
    public List<Asmt_Roster> findAll();

    /**
     *
     * @param usr - user to find all project rosters
     * @return
     */
    public List<Asmt_Roster> findAllByUsr(Usr usr);

    /**
     *
     * @param asmt assignment to find all project rosters by
     * @return
     */
    public List<Asmt_Roster> findAllByAsmt(Asmt asmt);

    /**
     *
     * @param ark
     * @return
     */
    public Asmt_Roster findByAr(Asmt_RosterKey ark);

    /** Save a new Project_Roster Responsibility
     *
     * @param pr the project roster entry to save
     * @return pr - the project roster entry we saved(useful for testing)
     */

    /** Save a new Assignment Grade
     *
     * @param g the assignment grade to save
     * @return g - the assignment grade we saved(useful for testing)
     */
    public Asmt_Roster save(Asmt_Roster g);

    /** Delete a user
     *
     * @param a - the assigment grade to delete
     */
    public void delete(Asmt_Roster a);
}

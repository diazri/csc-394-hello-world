package asmt_roster;

import asmt.Asmt;
import asmt.AsmtRepository;
import com.example.demo.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;
import usr.Usr;
import usr.UsrRepository;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes = {DemoApplication.class})
@EnableJpaRepositories(basePackages = {"usr", "project", "sub_project", "comment","asmt","asmt_roster", "project_roster", "sub_project_roster"})
public class Asmt_RosterRepositoryIntegrationTests {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private Asmt_RosterRepository asmtRosterRepository;

    @Autowired
    private AsmtRepository asmtRepository;

    @Autowired
    private UsrRepository usrRepository;

    @Test
    public void whenFindByARK_thenReturnAR() {
        // given
        Usr u1 = usrRepository.findByUsername("ggk");
        Asmt a1 = asmtRepository.findByAid(2);
        Asmt_RosterKey ark = new Asmt_RosterKey(2, "ggk");
        Asmt_Roster ar = new Asmt_Roster(ark, u1, a1, "A+");
        entityManager.persist(ar);
        entityManager.flush();

        // when
        Asmt_Roster found = asmtRosterRepository.findByAr(ar.getAsmtRoster());

        // then
        assertEquals(ar.getAsmtRoster().getAsmt(), found.getAsmtRoster().getAsmt());
        assertEquals(ar.getAsmtRoster().getStudent(), found.getAsmtRoster().getStudent());
    }

    @Test
    public void whenFindALL_thenReturnCorrectSize(){
        // when
        List<Asmt_Roster> found =asmtRosterRepository.findAll();

        // then
        assertEquals(2, found.size());
    }

    @Test
    public void whenSave_thenReturnCorrectSize(){
        // given
        Usr u1 = usrRepository.findByUsername("ggk");
        Asmt a1 = asmtRepository.findByAid(2);
        Asmt_RosterKey ark = new Asmt_RosterKey(2, "ggk");
        Asmt_Roster ar = new Asmt_Roster(ark, u1, a1, "A+");

        //when
        Asmt_Roster saved = asmtRosterRepository.save(ar);
        List<Asmt_Roster> found = asmtRosterRepository.findAll();

        // then
        assertEquals(3, found.size());
    }

    @Test
    public void whenSave_thenFindByARKReturnCorrectAR(){
        // given
        Usr u1 = usrRepository.findByUsername("ggk");
        Asmt a1 = asmtRepository.findByAid(2);
        Asmt_RosterKey ark = new Asmt_RosterKey(2, "ggk");
        Asmt_Roster ar = new Asmt_Roster(ark, u1, a1, "A+");

        //when
        Asmt_Roster saved = asmtRosterRepository.save(ar);
        Asmt_Roster found = asmtRosterRepository.findByAr(ark);

        // then
        assertEquals(ar.getAsmtRoster().getAsmt(), found.getAsmtRoster().getAsmt());
        assertEquals(ar.getAsmtRoster().getStudent(), found.getAsmtRoster().getStudent());
        assertEquals(ar.getGrade(), found.getGrade());
    }

    @Test
    public void whenSaveInvalidGrade_thenFail(){
        // given
        Usr u1 = usrRepository.findByUsername("ggk");
        Asmt a1 = asmtRepository.findByAid(2);
        Asmt_RosterKey ark = new Asmt_RosterKey(2, "ggk");
        Asmt_Roster ar = new Asmt_Roster(ark, u1, a1, "F---");

        //when
        try {
            Asmt_Roster saved = asmtRosterRepository.save(ar);
            entityManager.flush();
            // Then
            fail();
        }
        catch (Exception e){}
    }


    @Test
    public void whenDelete_thenFindAllSizeCorrect(){
        // given
        Usr u1 = usrRepository.findByUsername("ggk");
        Asmt a1 = asmtRepository.findByAid(2);
        Asmt_RosterKey ark = new Asmt_RosterKey(2, "ggk");
        Asmt_Roster ar = new Asmt_Roster(ark, u1, a1, "A+");

        entityManager.persist(ar);
        entityManager.flush();

        //when
        asmtRosterRepository.delete(ar);
        List<Asmt_Roster> found = asmtRosterRepository.findAll();

        // then
        assertEquals(2, found.size());
    }
}

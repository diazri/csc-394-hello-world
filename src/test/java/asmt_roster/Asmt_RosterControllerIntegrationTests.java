package asmt_roster;

import com.example.demo.DemoApplication;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class Asmt_RosterControllerIntegrationTests {
// ==== Test the context loading. ==== //

    @Autowired
    private Asmt_RosterController arcnt;

    @Test
    public void contextLoads() {
        assertNotNull(arcnt);
    }

    // ==== Test HTTP requests on dummy data. ==== //

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    // Instance variables for testing
    private String url;
    private String usrId;
    private int assignmentId;
    private Asmt_Roster dummy;

    @Before
    public void setup() {
        url = "http://localhost:" + port + "/assignmentRoster/";
        usrId = "ggk";
        assignmentId = 1;
        Asmt_RosterKey k = new Asmt_RosterKey(2, "ggk");
        dummy = new Asmt_Roster();
        dummy.setAsmtRosterKey(k);
        dummy.setGrade("D+");
    }

    @Test
    public void runTestsInOrder() throws JSONException {
        getAllARData();
        getDummyARData();
        postDummyARData();
        putDummyARData();
        deleteDummyARData();
    }

    //@Test
    public void getAllARData() throws JSONException {
        // Make an HTTP request using our url.
        String json = this.restTemplate.getForObject(url, String.class);

        // Make sure we got data back.
        assertNotNull(json);

        // Parse the JSON and make sure our array is the correct size.
        JSONArray arr = new JSONArray(json);
        assertEquals(2, arr.length());

        // Make sure the array contains the pairing between assignment 1 and user "ggk".
        assertTrue(json.contains("\"" + "asmt" + "\":" + assignmentId));
        assertTrue(json.contains("\"" + "student" + "\":\"" + usrId + "\""));
    }

    //@Test
    public void getDummyARData() throws JSONException {
        // Make an HTTP request using our url AND our primary key.
        String json = this.restTemplate.getForObject(url + usrId + "/" + assignmentId, String.class);

        // Make sure we got data back.
        assertNotNull(json);

        // Parse the JSON and make sure it's the user we're expecting.
        JSONObject ob = new JSONObject(json);
        ob = ob.getJSONObject("asmtRoster");
        assertEquals(assignmentId, ob.get("asmt"));
        assertEquals(usrId, ob.get("student"));
    }

    //@Test
    public void postDummyARData() throws JSONException {
        // Make an HTTP request using our url AND some dummy data.
        String json = this.restTemplate.postForObject(url, dummy, String.class);

        // Make sure we got data back.
        assertNotNull(json);

        // Parse the JSON and make sure it's the user we're expecting.
        JSONObject ob = new JSONObject(json);
        ob = ob.getJSONObject("asmtRoster");
        assertEquals(dummy.getAsmtRoster().getAsmt(), ob.get("asmt"));
        assertEquals(dummy.getAsmtRoster().getStudent(), ob.get("student"));
    }

    //@Test
    public void putDummyARData() throws JSONException {
        // Modify our dummy data to match an existing primary key.
        Asmt_RosterKey existing = new Asmt_RosterKey(assignmentId, usrId);
        dummy.setAsmtRosterKey(existing);

        // Make an HTTP request using our url AND some dummy data.
        this.restTemplate.put(url + usrId + "/" + assignmentId, dummy);

        // Get by primary key.
        String json = this.restTemplate.getForObject(url + usrId + "/" + assignmentId, String.class);

        // Make sure we got data back.
        assertNotNull(json);

        // Parse the JSON and make sure it's the user we're expecting.
        JSONObject ob = new JSONObject(json);
        JSONObject ar = ob.getJSONObject("asmtRoster");
        assertEquals(dummy.getAsmtRoster().getAsmt(), ar.get("asmt"));
        assertEquals(dummy.getAsmtRoster().getStudent(), ar.get("student"));
        assertEquals(dummy.getGrade(), ob.get("grade"));
    }

    //@Test
    public void deleteDummyARData() throws JSONException {
        // Make an HTTP request using our url AND the primary key we want to delete.
        this.restTemplate.delete(url + usrId + "/" + assignmentId);

        // Get the object we just deleted by primary key.
        String json = this.restTemplate.getForObject(url + usrId + "/" + assignmentId, String.class);

        // It should be null.
        assertNull(json);
    }
}

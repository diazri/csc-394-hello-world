package asmt;

import java.util.*;


import org.json.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;
import constants.Constants;
import project.Project;
import project.ProjectService;
import usr.Usr;
import usr.UsrService;

@RestController
public class AsmtController {

    @Autowired
    private AsmtService srv;

    @Autowired
    private UsrService usrv;

    @Autowired
    private ProjectService psrv;

    @GetMapping("/assignments")
    public @ResponseBody Collection<Asmt> findAll() { return srv.findAll();}

    @GetMapping(value = "/assignments/{aid}")
    public @ResponseBody Asmt findByAid(@PathVariable int aid){
        Asmt a = srv.findByAid(aid);
        return a;
    }

    @GetMapping(value = "/projects/{pid}/assignments")
    public @ResponseBody Collection<Asmt> findByProject(@PathVariable String pid) {
       try{
           int n = Integer.parseInt(pid);
           Project p = psrv.findByPID(n);
           return p.getAssignments();
       } catch(Exception e){
           return null;
       }

    }

    @PostMapping(value = "/assignments")
    public @ResponseBody Asmt save(@RequestBody String assignmentJson){
        try{
            // Instantiate a assignemnt
            // Parse the JSON.
            JSONObject j = new JSONObject(assignmentJson);

            // Populate all the data.
            int aid = (int) (System.currentTimeMillis() / 1000L);
            String aname = j.getString(Constants.ANAME);
            String owner = j.getString(Constants.OWNER);
            Usr usr = usrv.findByUsername(owner);
            int parent = j.getInt(Constants.PARENT);
            Project project = psrv.findByPID(parent);

            Asmt a = new Asmt(aid, usr, project, aname);
            return srv.save(a);
        } catch (Exception e) {
            // Something went wrong!
        	System.out.println("Request body: " + assignmentJson);
        	e.printStackTrace();
            return null;
        }
    }

    @PutMapping(value = "/assignments/{AID}")
    public @ResponseBody Asmt updateAssignment(@PathVariable int AID, @RequestBody String assignmentJSON){

        Asmt currentAssignment = srv.findByAid(AID);
        if(currentAssignment== null){
            System.out.println("Cant find project");
            return null;
        }
        try{
            // Instantiate a assignemnt
            // Parse the JSON.
            JSONObject j = new JSONObject(assignmentJSON);

            // Populate all the data.
            String aname = j.getString(Constants.ANAME);
            String owner = j.getString(Constants.OWNER);
            Usr usr = usrv.findByUsername(owner);
            int parent = j.getInt(Constants.PARENT);
            Project project = psrv.findByPID(parent);

            // Update the values
            currentAssignment.setProject(project);
            currentAssignment.setUsr(usr);
            currentAssignment.setAname(aname);

            srv.save(currentAssignment);
            return currentAssignment;
        } catch (Exception e) {
            // Something went wrong!
            System.out.println("Request body: " + assignmentJSON);
            e.printStackTrace();
            return null;
        }
    }
    
    @DeleteMapping(value = "/assignments/{aid}")
    public @ResponseBody void delete(@PathVariable int aid) {
    	srv.delete(aid);
    }
}


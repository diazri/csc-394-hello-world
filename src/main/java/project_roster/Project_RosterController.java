package project_roster;

import constants.Constants;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import project.Project;
import project.ProjectService;
import usr.Usr;
import usr.UsrService;

import java.util.Collection;
import java.util.List;

@RestController
public class Project_RosterController {

    @Autowired
    private Project_RosterService srv;

    @Autowired
    private ProjectService psrv;

    @Autowired
    private UsrService usrv;

    @GetMapping("/projectRoster")
    public @ResponseBody Collection<Project_Roster> findAll() { return srv.findAll();}

    @GetMapping("/projectRoster/user/{username}")
    public @ResponseBody Collection<Project_Roster> findAllByUser(@PathVariable String username){
        Usr usr = usrv.findByUsername(username);
        List<Project_Roster> lpr = srv.findAllByUsr(usr);
        return lpr;
    }

    @GetMapping("/projectRoster/project/{pid}")
    public @ResponseBody Collection<Project_Roster> findAllByProject(@PathVariable int pid){
        Project project = psrv.findByPID(pid);
        List<Project_Roster> lpr = srv.findAllByProject(project);
        return lpr;
    }

    @GetMapping("/projectRoster/{username}/{pid}")
    public @ResponseBody Project_Roster findByProject_RosterKey(@PathVariable String username, @PathVariable int pid){
        Project_RosterKey prk = new Project_RosterKey(pid, username);
        Project_Roster pr = srv.findByPr(prk);
        return pr;
    }

    @PostMapping(value = "/projectRoster")
    public @ResponseBody Project_Roster save(@RequestBody String projectrosterJson){
        try{
            // Instantiate the project Roster
            JSONObject j = new JSONObject(projectrosterJson);

            // Populate our fields.
            Project_RosterKey prk = new Project_RosterKey(j.getJSONObject(Constants.PRK).toString());
            String responsibility= j.getString(Constants.RESPONSIBILITY);
            String grade = j.getString(Constants.GRADE);




            Project project = psrv.findByPID(prk.getProject());
            Usr usr = usrv.findByUsername(prk.getStudent());

            Project_Roster pr = new Project_Roster(prk, usr, project, responsibility, grade);
            return srv.save(pr);
        } catch (Exception e){
            // Something went wrong!
            System.out.println("Request body: " + projectrosterJson);
            e.printStackTrace();
            return null;
        }
    }

    @PutMapping("/projectRoster/{username}/{pid}")
    public @ResponseBody Project_Roster updateProjectRoster(@PathVariable String username, @PathVariable int pid,
                                                            @RequestBody String projectRosterJSON)
    {
        // Get current project roster info
        Project_Roster currentProject_Roster = srv.findByPr(new Project_RosterKey(pid, username));
        if(currentProject_Roster == null){
            System.out.println("Cant find project");
            return null;
        }

        try{
            // Instantiate the project Roster
            System.out.println(projectRosterJSON);
            JSONObject j = new JSONObject(projectRosterJSON);


            String responsibility= j.getString(Constants.RESPONSIBILITY);
            String grade = j.getString( Constants.GRADE );


            //Update the Values
            currentProject_Roster.setResponsibility(responsibility);
            currentProject_Roster.setGrade(grade);

            return srv.save(currentProject_Roster);

        } catch (Exception e){
            // Something went wrong!
            System.out.println("Request body: " + projectRosterJSON);
            e.printStackTrace();
            return null;
        }
    }
    
    @DeleteMapping("/projectRoster/{username}/{pid}")
    public @ResponseBody void delete(@PathVariable String username, @PathVariable int pid) {
    	Project_RosterKey prk = new Project_RosterKey(pid, username);
    	srv.delete(prk);
    }
}

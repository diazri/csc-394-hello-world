package usr;

import static org.junit.Assert.*;

import org.json.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.DemoApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class UsrControllerTests {

// ==== Test the context loading. ==== //

	@Autowired
	private UsrController cnt;

	@Test
	public void contextLoads() {
		assertNotNull(cnt);
	}

// ==== Test HTTP requests on dummy data. ==== //

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	// Instance variables for testing
	private String url;
	private String primaryKey;
	private String id;
	private Usr dummy;

	@Before
	public void setup() {
		url = "http://localhost:" + port + "/users/";
		primaryKey = "username";
		id = "ggk";
		dummy = new Usr("j3f", "j3f@hotmail.com", "a-hashed-password", "nama", "jeff", "user");
	}

	@Test
	public void runTestsInOrder() throws JSONException {
		getAllDummyData();
		getDummyData();
		postDummyData();
		putDummyData();
		deleteDummyData();
	}
	
	//@Test
	public void getAllDummyData() throws JSONException {
		// Make an HTTP request using our url.
		String json = this.restTemplate.getForObject(url, String.class);

		// Make sure we got data back.
		assertNotNull(json);

		// Parse the JSON and make sure our array is the correct size.
		JSONArray arr = new JSONArray(json);
		assertEquals(4, arr.length());

		// Make sure the array contains the user "ggk".
		assertTrue(json.contains("\"" + primaryKey + "\":\"" + id + "\""));
	}

	//@Test
	public void getDummyData() throws JSONException {
		// Make an HTTP request using our url AND our primary key.
		String json = this.restTemplate.getForObject(url + id, String.class);

		// Make sure we got data back.
		assertNotNull(json);
		
		// Parse the JSON and make sure it's the user we're expecting.
		JSONObject ob = new JSONObject(json);
		assertEquals(id, ob.get(primaryKey));
	}
	
	//@Test
	public void postDummyData() throws JSONException {
		// Make an HTTP request using our url AND some dummy data.
		String json = this.restTemplate.postForObject(url, dummy, String.class);
		
		// Make sure we got data back.
		assertNotNull(json);
		
		// Parse the JSON and make sure it's the user we're expecting.
		JSONObject ob = new JSONObject(json);
		assertEquals(dummy.getUsername(), ob.get(primaryKey));
	}
	
	//@Test
	public void putDummyData() throws JSONException {
		// Modify our dummy data to match an existing primary key.
		dummy.setUsername(id);
		
		// Make an HTTP request using our url AND some dummy data.
		this.restTemplate.put(url + id, dummy);
		
		// Get by primary key.
		String json = this.restTemplate.getForObject(url + id, String.class);

		// Make sure we got data back.
		assertNotNull(json);
		
		// Parse the JSON and make sure it's the user we're expecting.
		JSONObject ob = new JSONObject(json);
		assertEquals(dummy.getFirst_name(), ob.get("first_name"));
	}
	
	//@Test
	public void deleteDummyData() throws JSONException {
		// Make an HTTP request using our url AND the primary key we want to delete.
		this.restTemplate.delete(url + id);
		
		// Get the object we just deleted by primary key.
		String json = this.restTemplate.getForObject(url + id, String.class);
		
		// It should be null.
		assertNull(json);
	}
}

package comment;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import usr.Usr;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(SpringRunner.class)
public class CommentServiceIntegrationTests {
    @TestConfiguration
    static class CommentServiceImplTestContextConfiguration {

        @Bean
        public CommentService commentService() {
            return new CommentService();
        }
    }

    @Autowired
    private CommentService commentService;

    @MockBean
    private CommentRepository commentRepository;

    // write test cases here
    @Before
    public void setUp() {
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        LocalDateTime ldt = LocalDateTime.now();
        Comment c1 = new Comment(7, david, "thumbs up", "Test comment 1", ldt, null, null, null);
        List<Comment> lc = new ArrayList<>();
        lc.add(c1);

        Mockito.when(commentRepository.findByCid(anyInt()))
                .thenReturn(c1);

        Mockito.when(commentRepository.findAll()).thenReturn(lc);
        Mockito.when(commentRepository.save(any(Comment.class)))
                .then(returnsFirstArg());


    }

    @Test
    public void whenValidCID_thenCommentShouldBeFound() {
        int cid = 7;
        Comment found = commentService.findByCid(7);

        assertEquals(cid, found.getCid());
    }

    @Test
    public void whenFind_thenReturnCorrectList() {
        List<Comment> cl = commentService.findAll();
        assertEquals(1, cl.size());
    }

    @Test
    public void whenSave_RecieveSameObject() {
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        LocalDateTime ldt = LocalDateTime.now();
        Comment c2 = new Comment(12, david, "thumbs up", "Test comment 1", ldt, null, null, null);

        Comment found = commentService.save(c2);

        assertEquals(12, found.getCid());
    }

    @Test
    public void whenDelete_thenVerify(){
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        LocalDateTime ldt = LocalDateTime.now();
        Comment c2 = new Comment(7, david, "thumbs up", "Test comment 1", ldt, null, null, null);
        commentService.delete(c2);

        Mockito.verify(commentRepository, Mockito.times(1)).delete(c2);
    }

    @Test
    public void whenDeleteById_thenVerify(){
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        LocalDateTime ldt = LocalDateTime.now();
        Comment c1 = new Comment(12, david, "thumbs up", "Test comment 1", ldt, null, null, null);
        commentService.delete(12);

        Mockito.verify(commentRepository, Mockito.times(1)).findByCid(12);
    }
}

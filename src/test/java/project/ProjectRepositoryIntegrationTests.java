package project;

import com.example.demo.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;
import usr.Usr;



import java.sql.Date;
import java.util.List;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes = {DemoApplication.class})
@EnableJpaRepositories(basePackages = {"usr", "project", "sub_project", "comment","asmt","asmt_roster", "project_roster", "sub_project_roster"})
public class ProjectRepositoryIntegrationTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProjectRepository projectRepository;

    @Test
    public void whenFindByPID_thenReturnProject() {
        // given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(7, david, "p1", "inProgress", new Date(0));
        entityManager.persist(p1);
        entityManager.flush();

        // when
        Project found = projectRepository.findByPID(p1.getPID());

        // then
        assertEquals(found.getPID(), p1.getPID());
    }

    @Test
    public void whenFindALL_thenReturnCorrectSize(){
        // when
        List<Project> found = projectRepository.findAll();

        // then
        assertEquals(2, found.size());
    }

    @Test
    public void whenSave_thenReturnCorrectSize(){
        // given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(7, david, "p1", "inProgress", new Date(0));

        //when
        Project saved = projectRepository.save(p1);
        List<Project> found = projectRepository.findAll();

        // then
        assertEquals(3, found.size());
    }

    @Test
    public void whenSave_thenFindByPidReturnCorrectPID(){
        // given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(7, david, "p1", "inProgress", new Date(0));


        //when
        Project saved = projectRepository.save(p1);
        Project found = projectRepository.findByPID(7);

        // then
        assertEquals(p1.getPID(),found.getPID());
    }

    @Test
    public void whenSaveInvalidUser_thenFail(){
        // given
        Usr david = new Usr("kkg","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(7, david, "p1", "inProgress", new Date(0));

        //when
        try {
            Project saved = projectRepository.save(p1);
            // Then
            fail();
        }
        catch (Exception e){}

    }

    @Test
    public void whenDelete_thenFindAllSizeCorrect(){
        // given
        Usr david = new Usr("ggk","dlkdavid@yahoo.com", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","david","kolo","prof");
        Project p1 = new Project(7, david, "p1", "inProgress", new Date(0));
        entityManager.persist(p1);
        entityManager.flush();

        //when
        projectRepository.delete(p1);
        List<Project> found = projectRepository.findAll();

        // then
        assertEquals(2, found.size());
    }
}

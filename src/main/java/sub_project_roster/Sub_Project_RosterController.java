package sub_project_roster;

import constants.Constants;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sub_project.Sub_Project;
import sub_project.Sub_ProjectService;
import usr.Usr;
import usr.UsrService;

import java.util.Collection;
import java.util.List;

@RestController
public class Sub_Project_RosterController {
    @Autowired
    private Sub_Project_RosterService srv;

    @Autowired
    private UsrService usrv;

    @Autowired
    private Sub_ProjectService spsrv;

    @GetMapping("/subprojectRoster")
    public @ResponseBody
    Collection<Sub_Project_Roster> findAll() { return srv.findAll();}

    @GetMapping("/subprojectRoster/user/{username}")
    public @ResponseBody Collection<Sub_Project_Roster> findAllByUser(@PathVariable String username){
        Usr usr = usrv.findByUsername(username);
        List<Sub_Project_Roster> lspr = srv.findAllByUsr(usr);
        return lspr;
    }

    @GetMapping("/subprojectRoster/subproject/{sid}")
    public @ResponseBody Collection<Sub_Project_Roster> findAllByProject(@PathVariable int sid){
        Sub_Project sub_project = spsrv.findBySid(sid);
        List<Sub_Project_Roster> lspr = srv.findAllSubProject(sub_project);
        return lspr;
    }

    @GetMapping("/subprojectRoster/{username}/{sid}")
    public @ResponseBody Sub_Project_Roster findByProject_RosterKey(@PathVariable String username, @PathVariable int sid){
        Sub_Project_RosterKey sprk = new Sub_Project_RosterKey(sid, username);
        Sub_Project_Roster spr = srv.findBySpr(sprk);
        return spr;
    }

    @PostMapping(value = "/subprojectRoster")
    public @ResponseBody Sub_Project_Roster save(@RequestBody String subprojectrosterJson){
        try{
            // Parse the Json

            JSONObject j = new JSONObject(subprojectrosterJson);

            // Populate our fields.
            Sub_Project_RosterKey sprk = new Sub_Project_RosterKey(j.getJSONObject(Constants.SPRK).toString());
            String responsibility = j.getString(Constants.RESPONSIBILITY);

            // Find the subproject and user associated with our key.

            Sub_Project sub_project = spsrv.findBySid(sprk.getSub_project());
            Usr usr = usrv.findByUsername(sprk.getStudent());

            // Instantiate a roster with the subproject and user we found, and save it.
            Sub_Project_Roster spr = new Sub_Project_Roster(sprk, usr, sub_project, responsibility);
            return srv.save(spr);
        } catch (Exception e){
            // Something went wrong!
            System.out.println("Request body: " + subprojectrosterJson);
            e.printStackTrace();
            return null;
        }
    }

    @PutMapping("/subprojectRoster/{username}/{sid}")
    public @ResponseBody Sub_Project_Roster updateProjectRoster(@PathVariable String username, @PathVariable int sid,
                                                            @RequestBody String subprojectRosterJSON)
    {
        // Get current project roster info
        Sub_Project_Roster currentSub_Project_Roster = srv.findBySpr(new Sub_Project_RosterKey(sid, username));
        if(currentSub_Project_Roster == null){
            System.out.println("Cant find project");
            return null;
        }

        try{
            // Instantiate the sub_project Roster
            System.out.println(subprojectRosterJSON);
            JSONObject j = new JSONObject(subprojectRosterJSON);


            String responsibility= j.getString(Constants.RESPONSIBILITY);


            //Update the Values
            currentSub_Project_Roster.setResponsibility(responsibility);

            return srv.save(currentSub_Project_Roster);

        } catch (Exception e){
            // Something went wrong!
            System.out.println("Request body: " + subprojectRosterJSON);
            e.printStackTrace();
            return null;
        }
    }

    @DeleteMapping(value = "subprojectRoster/{student}/{subproject}")
    public @ResponseBody void delete(@PathVariable String student, @PathVariable int subproject) {
        // Generate a key.
        Sub_Project_RosterKey sprk = new Sub_Project_RosterKey(subproject, student);

        // Delete by key.
        srv.delete(sprk);
    }
}

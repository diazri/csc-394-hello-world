package asmt_roster;

import asmt.Asmt;
import asmt.AsmtService;
import constants.Constants;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import usr.Usr;
import usr.UsrService;

import java.util.Collection;
import java.util.List;

@RestController
public class Asmt_RosterController {

    @Autowired
    private Asmt_RosterService srv;

    @Autowired
    private AsmtService asrv;

    @Autowired
    private UsrService usrv;

    @GetMapping("/assignmentRoster")
    public @ResponseBody Collection<Asmt_Roster> findAll(){ return srv.findAll();}

    @GetMapping("/assignmentRoster/user/{username}")
    public @ResponseBody Collection<Asmt_Roster> findAllByUser(@PathVariable String username){
        Usr usr = usrv.findByUsername(username);
        List<Asmt_Roster> lar = srv.findAllByUsr(usr);
        return lar;
    }

    @GetMapping("/assignmentRoster/assignment/{aid}")
    public @ResponseBody Collection<Asmt_Roster> findAllByAssignment(@PathVariable int aid){
        Asmt asmt = asrv.findByAid(aid);
        List<Asmt_Roster> lar = srv.findAllByAssignment(asmt);
        return lar;
    }

    @GetMapping("/assignmentRoster/{username}/{aid}")
    public @ResponseBody Asmt_Roster findByAsssignment_RosterKey(@PathVariable String username, @PathVariable int aid){
        Asmt_RosterKey ark = new Asmt_RosterKey(aid, username);
        Asmt_Roster ar = srv.findByAr(ark);
        return ar;
    }

    @PostMapping(value = "/assignmentRoster")
    public @ResponseBody Asmt_Roster save(@RequestBody String assignmentrosterJson){
        try{
            // Instantiate an assignemnt
            JSONObject j = new JSONObject(assignmentrosterJson);

            // Populate our fields.
            Asmt_RosterKey ark = new Asmt_RosterKey(j.getJSONObject(Constants.ARK).toString());
            String Grade = j.getString(Constants.GRADE);




            Asmt asmt = asrv.findByAid(ark.getAsmt());
            Usr usr = usrv.findByUsername(ark.getStudent());

            Asmt_Roster ag = new Asmt_Roster(ark, usr, asmt, Grade);
            return srv.save(ag);
        } catch (Exception e) {
            // Something went wrong!
        	System.out.println("Request body: " + assignmentrosterJson);
        	e.printStackTrace();
            return null;
        }
    }

    @PutMapping("/assignmentRoster/{username}/{aid}")
    public @ResponseBody Asmt_Roster updateProjectRoster(@PathVariable String username, @PathVariable int aid,
                                                            @RequestBody String assignmentRosterJSON)
    {
        // Get current project roster info
        Asmt_Roster currentAssignment_Roster = srv.findByAr(new Asmt_RosterKey(aid, username));
        if(currentAssignment_Roster == null){
            System.out.println("Cant find Assignment");
            return null;
        }

        try{
            // Instantiate the project Roster
            //System.out.println(assignmentRosterJSON);
            JSONObject j = new JSONObject(assignmentRosterJSON);


            String grade= j.getString(Constants.GRADE);


            //Update the Values
            currentAssignment_Roster.setGrade(grade);

            return srv.save(currentAssignment_Roster);

        } catch (Exception e){
            // Something went wrong!
            System.out.println("Request body: " + assignmentRosterJSON);
            e.printStackTrace();
            return null;
        }
    }
    
    @DeleteMapping("/assignmentRoster/{username}/{aid}")
    public @ResponseBody void delete(@PathVariable String username, @PathVariable int aid) {
    	Asmt_RosterKey k = new Asmt_RosterKey(aid, username);
    	srv.delete(k);
    }
}

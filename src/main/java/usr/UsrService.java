package usr;

import java.util.List;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

@Service
public class UsrService {

	@Autowired
	private UsrRepository repo;
	
	/** Returns every user in our database.
	 */
	public List<Usr> findAll() {
		return repo.findAll();
	}
	
	/** Find a specific user.
	 * 
	 * @param u - the username of the user we want to find.
	 * @return - the user (if any) that has the given username.
	 */
	public Usr findByUsername(String u) {
		return repo.findByUsername(u);
	}
	
	/** Save  a user.
	 * 
	 * @param u - the user to save.
	 * @return - the user we saved (useful for testing).
	 */
	public Usr save(Usr u) {
		return repo.save(u);
	}

	/**
	 *
	 * @param u - the user to update
	 * @return - user we updated
	 */

	public void delete(Usr u) {
		repo.delete(u);
	}
	
	/** Delete the user with a given username (if one exists).
	 * 
	 * @param username - the username to delete.
	 */
	public void delete(String username) {
		Usr u = findByUsername(username);
		delete(u);
	}
}

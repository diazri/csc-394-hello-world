package project_roster;

import constants.Constants;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Embeddable;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class Project_RosterKey implements Serializable {
    @Column(name="project")
    int project;

    @Column(name="student")
    String student;

    /**
     * Standard no argument constructor
     */
    public Project_RosterKey(){}

    /** Constructor
     *
     * @param project - comment id
     * @param student - student username
     */
    public Project_RosterKey(int project, String student){
        this.setProject(project);
        this.setStudent(student);
    }

    public Project_RosterKey(String json) throws JSONException {

        JSONObject j = new JSONObject(json);

        this.setProject(j.getInt(Constants.PROJECT));
        this.setStudent(j.getString(Constants.STUDENT));
    }

    /**
     *
     * @return project id
     */
    public int getProject(){
        return this.project;
    }

    /**
     *
     * @param project
     */
    public void setProject(int project){
        this.project = project;
    }

    /**
     *
     * @return student username
     */
    public String getStudent(){
        return this.student;
    }

    /**
     *
     * @param student
     */
    public void setStudent(String student){
        this.student = student;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj == this) return true;
        if(!(obj instanceof Project_RosterKey)) return false;

        Project_RosterKey that = (Project_RosterKey) obj;

        return this.getProject()==that.getProject() && this.getStudent().equals(that.getStudent());
    }

    @Override
    public int hashCode(){
        int hash = 7;

        hash = 31 * hash + this.getProject();
        hash = 31 * hash + (this.getStudent() == null ? 0 : this.getStudent().hashCode());
        return hash;
    }
}

package asmt;

import comment.Comment;
import constants.Constants;
import project.Project;
import sub_project_roster.Sub_Project_Roster;
import usr.Usr;

import javax.persistence.*;

import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnore;

import asmt_roster.Asmt_Roster;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
public class Asmt {

	// Primary Key
	@Id
	private int aid;

	// Foreign key to usr
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "owner", nullable = false)
	private Usr usr;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "project", nullable = false)
	private Project project;

	private String aname;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "asmt_comment", joinColumns = @JoinColumn(name = "asmt"), inverseJoinColumns = @JoinColumn(name = "comment_id"))
	Set<Comment> comments;

	// Auto-remove orphaned roster entries.
	@OneToMany(mappedBy = "asmt", orphanRemoval = true)
	private Set<Asmt_Roster> asmt_rosters;
	
	/**
	 * A no-argument constructor.
	 */
	public Asmt() {

	}

	/**
	 * A constructor.
	 *
	 * @param aid     - assignemnt id (integer)
	 * @param usr   - assignment owner (usr)
	 * @param project - parent project (project)
	 * @param aname   - assignment name (in plain text)
	 */
	public Asmt(int aid, Usr usr, Project project, String aname) {
		// Store assignment id
		this.setAid(aid);

		// Store owner
		this.setUsr(usr);

		// Store project
		this.setProject(project);

		// Store assignment name
		this.setAname(aname);
	}

	/**
	 *
	 * @return the assignemtn id
	 */
	public int getAid() {

		return aid;
	}

	/**
	 *
	 * @param aid the comment id to set
	 */
	public void setAid(int aid) {
		this.aid = aid;
	}

	/**
	 *
	 * @return the comment owners(usr)
	 */
	public Usr getUsr() {
		return usr;
	}

	/**
	 *
	 * @param usr owners(usr) of the assignment
	 */
	public void setUsr(Usr usr) {
		this.usr = usr;
	}

	/**
	 *
	 * @return the comment parent project(project)
	 */
	public Project getProject() {
		return project;
	}

	/**
	 *
	 * @param project the parent project(Project) to set
	 */
	public void setProject(Project project) {
		this.project = project;
	}

	/**
	 *
	 * @return the assignemnt name
	 */
	public String getAname() {
		return aname;
	}

	/**
	 *
	 * @param aname the name of the project to set
	 */
	public void setAname(String aname) {
		this.aname = aname;
	}

	/**
	 *
	 * @return assignment comments
	 */
	public Set<Comment> getComments() {
		return comments;
	}

	/**
	 *
	 * @param comments assignment comments to set
	 */
	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	/**
	 *
	 * @return
	 */
	public String getOwner() {return this.usr.getUsername();}

	public int getParent() {return this.project.getPID();}


}

package project;

import java.util.*;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

	/** Returns every project in our database.
	 */
	public List<Project> findAll();
	
	/** Find a specific project (by PID).
	 * 
	 * @param p - the id of the project we want to find.
	 * @return - the project (if any) that has the given PID.
	 */
	public Project findByPID(int p);

	/**
	 *
	 * @param p Project to save
	 * @return - The saved project
	 */
	public Project save(Project p);

	/** Delete a project (if it exists).
	 * 
	 * @param p - the project to delete.
	 */
	public void delete(Project p);
	
}

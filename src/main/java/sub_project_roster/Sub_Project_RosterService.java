package sub_project_roster;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sub_project.Sub_Project;
import usr.Usr;

import java.util.List;

@Service
public class Sub_Project_RosterService {

    @Autowired
    private Sub_Project_RosterRepository repo;

    /**
     *
     * @return every sub project roster entry in our database
     */
    public List<Sub_Project_Roster> findAll(){
        return repo.findAll();
    }

    /**
     *
     * @param usr
     * @return
     */
    public List<Sub_Project_Roster> findAllByUsr(Usr usr){return repo.findAllByUsr(usr);}

    /**
     *
     * @param sub_project
     * @return
     */
    public List<Sub_Project_Roster> findAllSubProject(Sub_Project sub_project){return repo.findAllBySubProject(sub_project);}

    /**
     *
     * @param sprk
     * @return
     */
    public Sub_Project_Roster findBySpr(Sub_Project_RosterKey sprk){return  repo.findBySpr(sprk);}

    /**
     *
     * @param spr
     * @return
     */
    public Sub_Project_Roster save(Sub_Project_Roster spr) {return repo.save(spr);}

    /** Delete a roster.
     */
    public void delete(Sub_Project_Roster spr) {
        repo.delete(spr);
    }

    /** Delete a roster by primary key.
     */
    public void delete(Sub_Project_RosterKey key) {
        Sub_Project_Roster spr = repo.findBySpr(key);
        repo.delete(spr);
    }

}

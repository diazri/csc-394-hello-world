package comment;

import java.util.List;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

@Service
public class CommentService {

    @Autowired
    private CommentRepository repo;

    /** Returns every comment in our database.
     */
    public List<Comment> findAll() {
        return repo.findAll();
    }

    /** Find a comment.
     *
     * @param c - the comment id of the comment we want to find.
     * @return - the user (if any) that has the given comment id.
     */
    public Comment findByCid(int c) {
        return repo.findByCid(c);
    }

    /** Save a comment.
     *
     * @param c - the comment to save.
     * @return - the comment we saved (useful for testing).
     */
    public Comment save(Comment c) {
        return repo.save(c);
    }

    /** Delete a comment.
     *
     * @param c - the comment to delete.
     */
    public void delete(Comment c) {
        repo.delete(c);
    }

    /** Delete by primary key.
     *
     * @param cid - the primary key of the comment we want to delete.
     */
    public void delete(int cid) {
        Comment c = findByCid(cid);
        delete(c);
    }

}
